package org.nflash.inputsourcedriver.webdatainputsourcedriver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.stream.Stream;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.BytesMessage;

import java.net.URL;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.nflash.runner.amqp.AMQPSender;
import org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp.AMQPOnceReceiver;
import org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp.AMQPReceiver; // TODO: move to common client
import org.nflash.runner.model.VisionInputRequest;
import org.nflash.server.model.InputSourceDataRequest;
import org.nflash.dataservice.client.model.RegisterDataFrameReply;

public class Main {
    
    private volatile boolean shutdown = false;
    
    public static void main(String [] args) throws Exception {
        Main m = new Main();
        m.start();
    }

    public void start() {
        this.shutdown = false;
        AMQPReceiver msgLoop = new AMQPReceiver(this);
        System.out.println("ISD context started.");
        try {
            msgLoop.start("input.webdatainputsourcedriver");
            System.out.println("ISD context stopped.");
        } catch (JMSException jmsException) {
            System.out.println("JMS Error :: " + jmsException.toString());
        } catch (IOException ioe) {
            System.out.println("IOException :: " + ioe.toString());
        } catch (Exception e) {
            System.out.println("Exception :: " + e.toString());
        }
    }
    
    // Scaling: this module should be installed on one node only, but that's not strictly required,
    // as there may be unforeseen usecases which benefit from multiple nodes fetching the data from same input...
    // However, when the NS starts, the compose-file can start one instance of this driver, and start running its
    // own message-loop.
    public void handleInputSourceMessage(Message message) throws IOException, JMSException {
        String jsonObj = ((TextMessage) message).getText();
        InputSourceDataRequest isdr = InputSourceDataRequest.fromJson(jsonObj);
        String urlAddress = isdr.getDataSourceUri(); // url where to fetch the data from.
        String targetQueue = isdr.getTargetQueue(); // queue where to send the fetched data to. FIXME: rename to topic
        handleUrl(urlAddress, targetQueue);
        // TODO: return correlationId to NS-ack-queue...
    }
    
    public void handleUrl(String urlAddress, String inputQueue) throws IOException {        
        try {
            URL url = new URL(urlAddress);
            BufferedImage image = ImageIO.read(url);
            String imageType = "jpg";
            
            // Convert to bytes.
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( image, imageType, baos );
            baos.flush();
            byte[] bytes = baos.toByteArray();
            baos.close();
            
            System.out.println("Loaded image :: " + urlAddress);
        
            System.out.println("Read data into buffer [" + bytes.length + "] bytes. Registering to data-service.");
            
            String queueName = "context"; // Name of the queue that DataService listens to. TODO: should be read from env-variables.
            String replyQueueName = "input.webdatainputsourcedriver.dataid"; // Name of the queue where DataService should send the reply to.
            String correlationId = "123"; // TODO: should autogenerate
            
            //
            // FIXME: use sender-client from dataService, not one from driver.
            //
            org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp.AMQPSender amqpSender = new org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp.AMQPSender();
            
            amqpSender.registerDataFrameToDataService(replyQueueName, bytes, correlationId);
            
            System.out.println("[OK] Data frame sent.");
            
            AMQPOnceReceiver receiver = new AMQPOnceReceiver();
            int timeout = 30000; // TODO: read from config or take as param.
            Message reply = receiver.receiveSync(replyQueueName, correlationId, timeout);
            
            System.out.println("[OK] Received reply.");
            
            String replyString = ((TextMessage) reply).getText();
            
            System.out.println("Reply: " + replyString);
            
            RegisterDataFrameReply rdfr = RegisterDataFrameReply.fromJson( replyString );
            
            System.out.println("Getting dataId.");
            String dataId = rdfr.getDataId();
            System.out.println("[OK] Received data ID :: " + dataId);
            
            // TODO: currently this supports only one type of request, but we want to support
            //       others as well. => Should check the input type somehow.
            //       In this case, we could take the type as parameter, e.g. this is a "Vision",
            //       which would scan for images, but it could also by e.g. Sound or Text or something else.
            String ackQueue = "input.webdatainputsourcedriver.ack";
            VisionInputRequest inputRequest = new VisionInputRequest();
            inputRequest.setDataId(dataId);
            inputRequest.setSourceName("WebInput");
            inputRequest.setSourceDataFrameUri(urlAddress);
            inputRequest.setAckQueue(ackQueue); // query-runner sends ack back to this queue after it expects the next data-frame.
            
            AMQPSender runnerAmqpSender = new org.nflash.runner.amqp.AMQPSender();
            runnerAmqpSender.sendToTopic(inputQueue, inputRequest.toJsonString());
            
            System.out.println("[OK] Sent to input-queue: " + inputQueue);
            
            // Wait for reply until sending new data-frame, otherwise would fill the DataService's memory fast.
            // NeuralRunner, which reads the data in, sends the ACK AFTER the query has been executed.
            AMQPOnceReceiver ackReceiver = new AMQPOnceReceiver();
            int ackTimeout = 120000; // TODO: read from config or take as param.
            Message ackMessage = ackReceiver.receiveSync(ackQueue, dataId, ackTimeout); // uses dataId as correlationId
            // Get the transactionId from ack-message:
            String messageBody = ((TextMessage) ackMessage).getText();
            String transactionId = messageBody; // TODO: require AckModel
            System.out.println("[OK] Received ack. Sending FLUSH-command, dataId : transactionId: " + dataId + ":" + transactionId);
            amqpSender.flushByTransactionId(dataId + ":" + transactionId);
            
            System.out.println("[OK] Sent FLUSH-command to data-service.");
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Image could not be loaded into memory :: " + urlAddress);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Image could not be loaded into memory :: " + urlAddress);
        }
        System.out.println("Done.");
    }
}