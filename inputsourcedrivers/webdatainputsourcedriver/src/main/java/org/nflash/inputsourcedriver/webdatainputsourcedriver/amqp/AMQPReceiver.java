package org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.nflash.inputsourcedriver.webdatainputsourcedriver.Main;
import org.nflash.runner.NflashContext;

//
// Adapted from https://github.com/apache/qpid-jms/blob/master/qpid-jms-examples/src/main/java/org/apache/qpid/jms/example/HelloWorld.java
//
//
// This is used to listen the input source-queue.
//
public class AMQPReceiver {
    private static final String USER = System.getenv("ARTEMIS_USERNAME");
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD");
    
    private Main inputContext;
    
    private volatile boolean shutdown = false;
    
    public AMQPReceiver(Main main) { this.inputContext = main; }
    
    public void start(String queueName) throws Exception {
        // The configuration for the Qpid InitialContextFactory has been supplied in
        // a jndi.properties file in the classpath, which results in it being picked
        // up automatically by the InitialContext constructor.
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        MessageConsumer messageConsumer = session.createConsumer(queue);
        System.out.println("Connected. Starting to listen input source queue :: " + queueName);
        
        int timeout = 1000;
        try {
            this.shutdown = false;
            while (!shutdown) {
                Message message = messageConsumer.receive(timeout);
                if (message != null) {
                    inputContext.handleInputSourceMessage(message);
                }
            }
        } finally {
            connection.close();
        }
    }

    public void shutdown() {
        this.shutdown = true;
    }
    
    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("AMQPReceiver :: Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}