package org.nflash.inputsourcedriver.webdatainputsourcedriver.amqp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.BytesMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class AMQPSender {
    private static final int DEFAULT_COUNT = 10;
    private static final int DELIVERY_MODE = DeliveryMode.NON_PERSISTENT;
    private static final String USER = System.getenv("ARTEMIS_USERNAME");
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD");

    public AMQPSender() { }

    // 
    // TODO: this method should be available through the client that DataService publishes.
    // 
    public void registerDataFrameToDataService(String replyQueueName, 
                    byte[] bytes, String correlationId) throws Exception {
        
        String queueName = "context";
        
        // The configuration for the Qpid InitialContextFactory has been supplied in
        // a jndi.properties file in the classpath, which results in it being picked
        // up automatically by the InitialContext constructor.
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        Destination replyQueue = session.createQueue(replyQueueName);

        MessageProducer messageProducer = session.createProducer(queue);
        long messageTimeToLive = 60000;

        BytesMessage message = session.createBytesMessage();
        message.clearBody();
        message.writeBytes(bytes);
        message.setJMSType("BYTES");
        message.setJMSCorrelationID(correlationId);
        message.setJMSReplyTo(replyQueue);
        message.reset();
        messageProducer.send(message, DELIVERY_MODE, Message.DEFAULT_PRIORITY, messageTimeToLive);

        connection.close();
    }

    public void flushByTransactionId(String transactionId) throws Exception {
        String queueName = "context";
        Context context = new InitialContext();
        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");
        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination queue = session.createQueue(queueName);
        MessageProducer messageProducer = session.createProducer(queue);
        TextMessage message = session.createTextMessage(transactionId);
        message.setJMSType("FLUSH");
        messageProducer.send(message);
        connection.close();
    }

    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("AMQPSender :: Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}