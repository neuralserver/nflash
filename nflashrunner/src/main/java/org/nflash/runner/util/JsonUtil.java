package org.nflash.runner.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Map;
import java.io.IOException;

// import ANTLR's runtime libraries
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.nflash.parser.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.nflash.runner.node.json.*;
import org.nflash.runner.NflashCompiler;
import org.nflash.runner.NflashContext;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;

/**
 *
 *
 */
public class JsonUtil {

    public static JsonValue jsonValueFromString(String json) {
        try {
            InputStream is = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
            
            // Create a CharStream that reads from standard input
            ANTLRInputStream input = new ANTLRInputStream(is);

            // create a lexer that feeds off of input CharStream
            NflashLexer lexer = new NflashLexer(input);
            
            // create a buffer of tokens pulled from the lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            
            // create a parser that feeds off the tokens buffer
            NflashParser parser = new NflashParser(tokens);
            ParseTree tree = parser.json(); // begin parsing at json rule
            
            // Create a generic parse tree walker that can trigger callbacks
            ParseTreeWalker walker = new ParseTreeWalker();
            
            // Walk the tree created during the parse, trigger callbacks
            NflashCompiler compiler = new NflashCompiler();
            
            NflashContext jsonContext = new NflashContext();
            Statement jsonStatement = new DefaultStatement(jsonContext);
            compiler.setCurrentStatement(jsonStatement);
            walker.walk(compiler, tree);
            
            // Extract the compiled value.
            return (JsonValue) compiler.getCurrentStatement().getNode();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("RETURNING NULL");
        return null;
    }
    
}