package org.nflash.runner;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import org.nflash.runner.model.InputSource;
import org.nflash.runner.amqp.AMQPReceiver;
import org.nflash.runner.amqp.AMQPSender;
import org.nflash.runner.util.RandomUtil;

import org.nflash.runner.model.VisionInputRequest;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.FunctionStatement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.util.JsonUtil;

/**
 * 
 * Context -class acts as a container for the query elements.
 * 
 */
public class NflashContext {

    private Map<String, JsonValue> runtimeValues;
    private InputSource inputSource;
    
    private Map<String, FunctionStatement> functionStatements;
    private Map<String, Statement> letStatements;
    private Statement selectStatement;
    
    private AMQPReceiver msgLoop;
    private volatile boolean shutdown = false;

    private String contextId;
    private String transactionId;
    
    public NflashContext() throws IOException {
        inputSource = new InputSource();
        this.runtimeValues = new HashMap<String, JsonValue>();
        this.functionStatements = new HashMap<String, FunctionStatement>();
        this.letStatements = new HashMap<String, Statement>();
    }

    public InputSource getInputSource() { return inputSource; }

    public void start() {
        this.shutdown = false;
        msgLoop = new AMQPReceiver(this);
        System.out.println("Nflash context started.");
        try {
            msgLoop.start(this.getInputSource().getName());
            System.out.println("Nflash context stopped.");
        } catch (JMSException jmsException) {
            System.out.println("JMS Error :: " + jmsException.toString());
        } catch (IOException ioe) {
            System.out.println("IOException :: " + ioe.toString());
        } catch (Exception e) {
            System.out.println("Exception :: " + e.toString());
        }
    }
    
    public void shutdown() {
        msgLoop.shutdown();
        this.shutdown = true;
    }
    
    public void handleInputSourceMessage(Message message) throws Exception, JMSException, IOException {
        if (message == null) {
            return;
        }
        // 
        // Create new transactionId, which is used until the data for the query has been evaluated.
        // TransactionId is used to flush the DataService's memory.
        // 
        transactionId = RandomUtil.randomAlphaNumeric(12);
        String messageBody = ((TextMessage) message).getText();
        
        // TODO: assumes, that this is VisionInputRequest, but could be anything else aswell.
        VisionInputRequest input = VisionInputRequest.fromJson(messageBody);
        String ackQueue = input.getAckQueue();
        String dataId = input.getDataId();
        
        // Register dataframe to context:
        JsonValue dataFrameJson = JsonUtil.jsonValueFromString(input.toJsonString());
        runtimeValues.put(inputSource.getBindingVariable(), dataFrameJson); // bindingVariable, e.g. "wa"
        runtimeValues.put("$", dataFrameJson); // rootValue
        runtimeValues.put("@", dataFrameJson); // currentValue
        
        System.out.println("Received message :: " + input.toJsonString()); // TODO: use logger instead
        
        selectStatement.setCurrentValue(JsonUtil.jsonValueFromString(input.toJsonString()));

        System.out.println("NflashContext :: ENTER SELECT.EVALUATE()");
        
        JsonValue selectResult = (JsonValue) this.getSelectStatement().evaluate();
        
        System.out.println("NflashContext :: EXIT SELECT.EVALUATE()");
        
        System.out.println(selectResult);
        
        // Send to output-queue, which the UI could listen to
        System.out.println("=================================");
        System.out.println("Sending result to output-queue :: " + selectResult);
        System.out.println("=================================");
        AMQPSender sender = new AMQPSender();
        sender.sendToQueue("nql_output_" + getContextId(), selectResult.toString());
        
        // Send ack:
        // transactionId is required because one dataId may generate multiple sub-data items in the data-service,
        // which will all be associated with single transactionId. When the query has completed, they will all
        // be removed from the memory, by driver (otherwise they would fill up the memory).
        sender.sendToQueueCorrelated(ackQueue, transactionId, dataId); // use dataId as correlationId. TODO: ackMessage -object (model)
        
        //
        // NOTE: driver will unregister dataframe(s) from data-service, by transactionId, which will release used memory.
        //
        System.out.println("Done.");
    }

    public void setContextId(String id) {
        this.contextId = id;
    }
    
    public String getContextId() {
        return this.contextId;
    }
    
    public void setTransactionId(String id) {
        this.transactionId = id;
    }
    
    public String getTransactionId() {
        return this.transactionId;
    }
    
    public Map<String, JsonValue> getRuntimeValues() {
        return runtimeValues;
    }
    
    public void setSelectStatement(Statement select) {
        this.selectStatement = select;
    }
    
    public Statement getSelectStatement() {
        return this.selectStatement;
    }

    public Map<String, FunctionStatement> getFunctionStatements() {
        return this.functionStatements;
    }
    
    public Map<String, Statement> getLetStatements() {
        return this.letStatements;
    }
}