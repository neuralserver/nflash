package org.nflash.runner.model;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

public class NeuralModelCallRequest {
    private String transactionId;
    private Map<String, String> input;
    private Map<String, List<String>> params;
    
    public NeuralModelCallRequest() {}
    
    public static NeuralModelCallRequest setFromJson(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, NeuralModelCallRequest.class);
    }
    
    public void setTransactionId(String txId) {
        this.transactionId = txId;
    }
    
    public String getTransactionId() {
        return this.transactionId;
    }
    
    public void setInput(Map<String, String> jsonInput) {
        this.input = jsonInput;
    }
    
    public Map<String, String> getInput() {
        return this.input;
    }
    
    public void setParams(Map<String , List<String>> jsonParams) {
        this.params = jsonParams;
    }
    
    public Map<String, List<String>> getParams() {
        return this.params;
    }
    
    public String toJsonString() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }
}