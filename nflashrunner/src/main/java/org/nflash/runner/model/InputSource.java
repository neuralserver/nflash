package org.nflash.runner.model;

public class InputSource {

    private String system; // E.g. Vision, Memory
    private String name;
    private String bindingVariable;

    public InputSource() {}

    public void setSystem(String sys) {system = sys;}
    public void setName(String n) {name = n;}
    public void setBindingVariable(String b) {bindingVariable = b;}
    
    public String getSystem() {return system;}
    public String getName() {return name;}
    public String getBindingVariable() {return bindingVariable;}

}