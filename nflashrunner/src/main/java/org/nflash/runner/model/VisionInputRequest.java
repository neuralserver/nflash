package org.nflash.runner.model;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

public class VisionInputRequest {
    private String dataId; // DataId for registered dataframe, given by DataService. Refers to associated bytebuffer.
    private String sourceName; // Link to input-source driver-name. E.g. folder-input-source
    private String sourceDataFrameUri; // propagated throughout the query. Maintains link to original source.
    private String ackQueue; // Name of the queue where the query sends ack-message when the query has completed processing this dataframe and is expecting to receive next.
    
    public VisionInputRequest() {}
    
    public static VisionInputRequest fromJson(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, VisionInputRequest.class);
    }
    
    public void setDataId(String dataId) {
        this.dataId = dataId;
    }
    
    public String getDataId() {
        return this.dataId;
    }

    public void setSourceName(String sn) {
        this.sourceName = sn;
    }
    
    public String getSourceName() {
        return this.sourceName;
    }

    public void setSourceDataFrameUri(String sourceDataFrameUri) {
        this.sourceDataFrameUri = sourceDataFrameUri;
    }
    
    public String getSourceDataFrameUri() {
        return this.sourceDataFrameUri;
    }

    public String toJsonString() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

    public void setAckQueue(String ackQueue) {
        this.ackQueue = ackQueue;
    }
    
    public String getAckQueue() {
        return this.ackQueue;
    }
}