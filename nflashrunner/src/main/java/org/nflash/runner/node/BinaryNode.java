package org.nflash.runner.node;

import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;
//import org.nflash.runner.node.json.JsonValue;

public class BinaryNode extends AbstractNode implements Node {
    
    private Node lhs;
    private Node rhs;
    private BinaryNodeProcessor proc;
    private Node evaluatedValue;
    
    public BinaryNode(Statement stmnt) {
        super(stmnt);
    }

    public Node evaluate() throws Exception {
        Node result = proc.process(this.getStatement(), lhs, rhs);
        this.setEvaluatedValue(result);
        return result;
    }
    
    public void setLhs(Node lhs) { this.lhs = lhs; }
    public void setRhs(Node rhs) { this.rhs = rhs; }
    public void setBinaryNodeProcessor(BinaryNodeProcessor proc) {this.proc = proc; }
    
    public void setEvaluatedValue(Node value) {
        this.evaluatedValue = value;
    }

    public Node getEvaluatedValue() {
        return this.evaluatedValue;
    }
    
    public String toString() {
        return this.getEvaluatedValue().toString();
    }
}
