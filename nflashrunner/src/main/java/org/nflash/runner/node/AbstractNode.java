package org.nflash.runner.node;

import org.nflash.runner.statement.Statement;

public abstract class AbstractNode implements Node {
    
    private Statement statement;

    public AbstractNode(Statement statement) {
        this.statement = statement;
    }
    
    public Node evaluate() throws Exception {
        return null;
    }

    public Statement getStatement() {
        return this.statement;
    }

}
