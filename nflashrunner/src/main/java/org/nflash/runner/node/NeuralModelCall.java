package org.nflash.runner.node;

import org.nflash.runner.NflashContext;
import org.nflash.runner.amqp.AMQPSender;
import org.nflash.runner.amqp.AMQPOnceReceiver;
import org.nflash.runner.util.RandomUtil;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import org.apache.qpid.proton.amqp.Binary;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.AbstractNode;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;

import org.nflash.runner.model.NeuralModelCallRequest;
import org.nflash.runner.util.JsonUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

//
// TODO: Change to "implements Node" instead of Processor
//
public class NeuralModelCall extends AbstractNode implements Node {

    private String lhsJsonObjRef; // assigned from parsed static query. Actual referred value is loaded in the evaluate -function.
    private String rhsTargetModel; // assigned from parsed static query: neural model (used as queue-name)
    private JsonValue jsonConfiguration; // optional: json-configuration for the neural model
    
    public NeuralModelCall(Statement statement) {
        super(statement);
    }

    public Node evaluate() throws Exception {
        JsonValue currentValue = this.getStatement().getCurrentValue();
        System.out.println("Sending " + currentValue.toString() + " to " + rhsTargetModel);
        
        AMQPSender sender = new AMQPSender();
        
        String forwardQueueName = rhsTargetModel; // indirection: send the reply to neural model
        String replyQueueName = rhsTargetModel + "_reply";
        String correlationId = RandomUtil.randomAlphaNumeric(10);
        
        System.out.println("NM CALL==================");
        System.out.println("REPLY QUEUE :: " + replyQueueName);
        System.out.println("=========================");
        
        NeuralModelCallRequest nmCallRequest = new NeuralModelCallRequest(); // FIXME: use native JsonValue instead
        
        // No json_configuration given for the neural-model:
        if (getJsonConfiguration() == null) {
            String neuralModelCallRequest = "{\"input\": " + currentValue.toString() + ",\"params\": {} }";
            nmCallRequest = NeuralModelCallRequest.setFromJson(neuralModelCallRequest);
        }
        
        // Send in the message also the json_configuration for the neural model
        else {
            String neuralModelCallRequest = "{\"input\": " + currentValue.toString() + ",\"params\": " + getJsonConfiguration().toString() + "}";
            nmCallRequest = NeuralModelCallRequest.setFromJson(neuralModelCallRequest);
        }
        
        String transactionId = this.getStatement().getNflashContext().getTransactionId();
        nmCallRequest.setTransactionId(transactionId);
        
        //
        // Send the query first to the data-service ("context" -queue), which pulls 
        // and assigns the required data-content. Context-server forwards the message
        // to the neural-model, and assigns the correct reply-queue for that model.
        // 
        // FIXME: use name DataService instead of ContextServer.
        sender.sendToContextServer("context", forwardQueueName, nmCallRequest, correlationId, transactionId);
        System.out.println("Blocking");
        
        // 
        // Start to block, wait for the corresponding response from the neural-model.
        // 
        AMQPOnceReceiver receiver = new AMQPOnceReceiver();
        Message message = receiver.receiveSync(replyQueueName, correlationId, 30000); // 30 second timeout
        
        // If receives empty list, then throws an exception:
        // Exception :: java.lang.ClassCastException: java.util.Collections$EmptyList cannot be cast to java.util.ArrayList
        // This takes care of that. Check if there is a better way to handle this.
        String className = ((ObjectMessage) message).getObject().getClass().getName();
        List<String> callResult = new ArrayList<String>(); // Collections.emptyList();
        Object obj = ((ObjectMessage) message).getObject();
        
        System.out.println("ClassName :: " + className);
        
        // Result is of type "java.util.ArrayList<org.apache.qpid.proton.amqp.Binary>", unless empty, when it is as below:
        if (!className.equals("java.util.Collections$EmptyList")) {
            //System.out.println("ADDING RESULT FROM RESPONSE.");
            
            ArrayList<org.apache.qpid.proton.amqp.Binary> objArr = (ArrayList<org.apache.qpid.proton.amqp.Binary>) obj;
            
            //System.out.println("RECEIVED RESPONSE :: " + objArr.toString());
            
            for (org.apache.qpid.proton.amqp.Binary bin : objArr) {
                String binString = bin.toString();
                callResult.add(binString);
            }
            //System.out.println("ADDED ALL :: " + result);
        }

        NeuralModelCallResultHolder nmCallResult = new NeuralModelCallResultHolder(callResult); // FIXME: no need for this
        String resultString = nmCallResult.getResult().toString();
        // update the currentValue from the statement
        this.getStatement().setCurrentValue(JsonUtil.jsonValueFromString(resultString));
        
        JsonValue result = JsonUtil.jsonValueFromString(resultString);
        System.out.println("NMCALL RESULT :: " + result);
        return result;
    }

    public void setLhsJsonObjRef(String jsonObjRef) { lhsJsonObjRef = jsonObjRef; }
    public void setRhsTargetModel(String targetModel) { rhsTargetModel = targetModel; }
    public void setJsonConfiguration(JsonValue jsonConfig) { this.jsonConfiguration = jsonConfig; }
    public String getLhsJsonObjRef() { return lhsJsonObjRef; }
    public String getRhsTargetModel() { return rhsTargetModel; }
    public JsonValue getJsonConfiguration() { return this.jsonConfiguration; }
    
    // FIXME: no need for this, at least in this form.
    public static class NeuralModelCallResultHolder {
        private List<String> result;
        public NeuralModelCallResultHolder(List<String> res) { result = res; }
        public List<String> getResult() { return result; }
    }

}