package org.nflash.runner.node;

import java.util.List;
import java.util.ArrayList;
import org.nflash.runner.statement.Statement;

public class MultiNode extends AbstractNode implements Node {
    
    private List<Node> nodes;
    private Node evaluatedNode;
    
    public MultiNode(Statement stmnt) {
        super(stmnt);
        this.nodes = new ArrayList<Node>();
    }

    public Node evaluate() throws Exception {
        // loop through nodes, and evaluate each
        Node result = null;
        System.out.println("ENTER MULTINODE EVALUATE");
        // Evaluate in reverse order
        for (int i = this.nodes.size() - 1; i >= 0; i --) {
            Node node = this.nodes.get(i);
            result = node.evaluate();
        }
        this.setEvaluatedNode(result);
        System.out.println("EXIT MULTINODE EVALUATE");
        return result;
    }
    
    public void addNode(Node n) {
        this.nodes.add(n);
    }
    
    public List<Node> getNodes() {
        return this.nodes;
    }
    
    public void setEvaluatedNode(Node n) {
        this.evaluatedNode = n;
    }
    
    public Node getEvaluatedNode() {
        return this.evaluatedNode;
    }
    
    public String toString() {
        return this.getEvaluatedNode().toString();
    }
}
