package org.nflash.runner.node;

import java.util.List;
import java.util.ArrayList;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonTrue;
import org.nflash.runner.util.JsonUtil;

public class Condition extends AbstractNode implements Node {

    private List<Node> whens;
    private List<Node> thens;
    private Node otherwise;
    private JsonValue evaluatedValue;
    
    public Condition(Statement stmnt) {
        super(stmnt);
        this.whens = new ArrayList<Node>();
        this.thens = new ArrayList<Node>();
    }

    public void addWhen(Node when) {
        this.whens.add(when);
    }
    
    public void addThen(Node then) {
        this.thens.add(then);
    }
    
    public void setOtherwise(Node otherwise) {
        this.otherwise = otherwise;
    }
    
    public Node getOtherwise() {
        return this.otherwise;
    }
    
    public List<Node> getWhens() {
        return this.whens;
    }
    
    public List<Node> getThens() {
        return this.thens;
    }
    
    public void setEvaluatedValue(JsonValue value) {
        this.evaluatedValue = value;
    }
    
    public JsonValue getEvaluatedValue() {
        return this.evaluatedValue;
    }
    
    public Node evaluate() throws Exception {
        System.out.println("ENTER Condition.evaluate()");
        List<Node> whensList = this.getWhens();
        JsonValue result = null;
        for (int i = whensList.size() - 1; i >= 0; i --) {
            Node when = whensList.get(i);
            JsonValue whenEvaluated = (JsonValue) when.evaluate();
            if (whenEvaluated instanceof JsonTrue) {
                Node then = this.getThens().get(i);
                result = (JsonValue) then.evaluate();
                this.setEvaluatedValue(result);
                break;
            }
        }
        if (this.getEvaluatedValue() == null) {
            Node other = this.getOtherwise();
            result = (JsonValue) other.evaluate();
            this.setEvaluatedValue(result);
        }
        return result;
    }

    public String toString() {
        return this.evaluatedValue.toString();
    }
}