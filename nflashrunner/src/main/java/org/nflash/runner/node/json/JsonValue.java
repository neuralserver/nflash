package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.AbstractNode;
import org.nflash.runner.util.JsonUtil;

public class JsonValue extends AbstractNode implements Node {

    private Node value;
    
    public JsonValue(Statement stmnt) {
        super(stmnt);
    }

    public void setValue(Node value) {
        // Value is either:
        //   JsonObj, JsonArray, StringNode, NumberNode, NullNode, BooleanNode
        if (value == null) {
            System.out.println("=== WARNING :: SETTING NULL VALUE !!! ===");
        }
        this.value = value;
    }

    public Node evaluate() throws Exception {
        Node result = value.evaluate();
        // update the currentValue from the statement
        this.getStatement().setCurrentValue((JsonValue) result);
        System.out.println("SET CurrentValue :: " + result.toString());
        return result;
    }
    
    public Node getValue() {
        return this.value;
    }
    
    public JsonValue copy() {
        return JsonUtil.jsonValueFromString(this.toString());
    }
    
    public String toString() {
        if (this.getValue() == null) {System.err.println("NULL VALUE IN JsonValue !!!");} // TODO: remove, only in debugging
        return this.getValue().toString();
    }
}