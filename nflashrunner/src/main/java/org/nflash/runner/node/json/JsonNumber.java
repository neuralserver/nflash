package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;

public class JsonNumber extends JsonValue implements Node {

    private Double value;
    
    public JsonNumber(Statement stmnt) {
        super(stmnt);
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Node evaluate() throws Exception {
        return this;
    }
    
    public JsonValue getValue() {
        return this;
    }
    
    public Double getNumberValue() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.getNumberValue());
    }
}