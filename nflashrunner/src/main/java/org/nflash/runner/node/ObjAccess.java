package org.nflash.runner.node;

import java.util.List;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.util.JsonUtil;

public class ObjAccess extends AbstractNode implements Node {

    private String objAccessKey;
    private JsonValue evaluatedValue;
    
    public ObjAccess(Statement stmnt) {
        super(stmnt);
    }

    public void setObjAccessKey(String key) {
        this.objAccessKey = key;
    }

    public Node evaluate() throws Exception {
        System.out.println("ENTER ObjAccess.evaluate()");
        
        // get currentValue from the statement
        JsonValue value = this.getStatement().getCurrentValue();
        System.out.println("    >> @: " + value.toString());

        if (value.evaluate() instanceof JsonObj) {
            System.out.println("EXIT ObjAccess.evaluate() obj");
            return evaluateObj( (JsonObj) value.evaluate() );
        }
        
        else if (value.evaluate() instanceof JsonArray) {
            System.out.println("EXIT ObjAccess.evaluate() array");
            return evaluateArray( (JsonArray) value.evaluate() );
        }
        System.out.println("ObjAccess: cannot access object. Wrong type: " + value.evaluate());
        throw new Exception("ObjAccess: cannot access object. Wrong type");
    }
    
    private Node evaluateObj(JsonObj obj) throws Exception {
        Node result = null;
        
        System.out.println("Accessing: " + this.getObjAccessKey());
        
        for (JsonPair pair : obj.getPairs()) {
            System.out.println("KEY :: " + pair.getKey());
            if (pair.getKey().equals("\"" + this.getObjAccessKey() + "\"")) {
                result = pair.getEvaluatedValue();
                break;
            }
        }
        
        // If not found, return empty JsonObj
        if (result == null) {
            result = new JsonObj(this.getStatement());
        }
        
        // update the currentValue from the statement
        this.getStatement().setCurrentValue((JsonValue) result);
        
        this.evaluatedValue = (JsonValue) result;
        return result;
    }
    
    private Node evaluateArray(JsonArray array) throws Exception {
        Node result = null;
        
        System.out.println("Accessing array of objects: " + this.getObjAccessKey());
        
        JsonArray resultArray = new JsonArray(this.getStatement());
        
        List<Node> arrayValues = array.getValues();
        
        for (int i = 0; i < arrayValues.size(); i ++) {
            Node arrayNode = arrayValues.get(i);
            System.out.println("    >> Looping: " + i);
            if (arrayNode.evaluate() instanceof JsonObj) {
                Node obj = evaluateObj((JsonObj) arrayNode.evaluate());
                resultArray.addValue(obj);
            }
        }
        
        // update the currentValue from the statement
        this.getStatement().setCurrentValue((JsonValue) resultArray);
        
        this.evaluatedValue = (JsonValue) resultArray;
        return resultArray;
    }
    
    public String getObjAccessKey() {
        return this.objAccessKey;
    }
    
    public String toString() {
        return this.evaluatedValue.toString();
    }
}