package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.Node;

public class JsonString extends JsonValue implements Node {

    private String value;
    
    public JsonString(Statement stmnt) {
        super(stmnt);
    }

    public Node evaluate() throws Exception {
        return this;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public JsonValue getValue() {
        return this;
    }
    
    public String getStringValue() {
        return this.value;
    }
    
    public String getJavaStringValue() {
        return this.value.substring(1, this.value.length() - 1);
    }
    
    @Override
    public String toString() {
        return this.getStringValue();
    }
}