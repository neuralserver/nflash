package org.nflash.runner.node;

import java.util.List;
import java.util.ArrayList;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.FunctionStatement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.util.JsonUtil;

public class FunctionCall extends AbstractNode implements Node {

    private String functionFQName;

    private FunctionStatement functionStatement; // function execution context
    
    private List<Node> params;
    
    private Node evaluatedValue;
    
    public FunctionCall(Statement stmnt) {
        super(stmnt);
        this.params = new ArrayList<Node>();
    }

    public Node evaluate() throws Exception {
        System.out.println("ENTER FunctionCall.evaluate()");
        FunctionStatement fnStat = this.getStatement().getNflashContext()
                                    .getFunctionStatements().get(this.getFunctionFQName());
        
        if (fnStat == null) {
            throw new Exception("No function with matching signature.");
        }

        List<String> arguments = fnStat.getArguments();

        // Set the fnStat's runtimeValues with the given parameters, by matching the correct order
        for (int i = 0; i < arguments.size(); i ++) {
            Node param = this.getParams().get(i);
            if (!(param instanceof JsonValue)) {
                param = param.evaluate();
            }
            String argName = arguments.get(i);
            fnStat.getRuntimeValues().put(argName, (JsonValue) param);
        }
        System.out.println("    >> ARGS SET: " + fnStat.getRuntimeValues().keySet().toString());
        this.evaluatedValue = fnStat.getNode().evaluate();
        this.getStatement().setCurrentValue((JsonValue) evaluatedValue);
        return this.evaluatedValue;
    }

    public void setFunctionFQName(String fqn) {
        this.functionFQName = fqn;
    }
    
    public String getFunctionFQName() {
        return this.functionFQName;
    }
    
    public FunctionStatement getFunctionStatement() {
        return this.functionStatement;
    }
    
    public List<Node> getParams() {
        return this.params;
    }
    
    public String toString() {
        return this.evaluatedValue.toString();
    }
}