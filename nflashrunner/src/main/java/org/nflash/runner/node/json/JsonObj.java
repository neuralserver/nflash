package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.AbstractNode;
import org.nflash.runner.node.json.JsonValue;
import java.util.List;
import java.util.ArrayList;

public class JsonObj extends JsonValue implements Node {

    private List<JsonPair> pairs;
    
    public JsonObj(Statement stmnt) {
        super(stmnt);
        this.pairs = new ArrayList<JsonPair>();
    }

    public void addPair(JsonPair pair) {
        this.pairs.add(pair);
    }

    public Node evaluate() throws Exception {
        JsonValue initialValue = this.getStatement().getCurrentValue();
        // @ might be null when executing only select statement (from unit-tests)
        if (initialValue != null) {
            initialValue = initialValue.copy();
        }
        for (JsonPair pair : this.getPairs()) {
            this.getStatement().setCurrentValue(initialValue);
            pair.evaluate();
        }
        return this;
    }
    
    public List<JsonPair> getPairs() {
        return this.pairs;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("{");
        
        for (int i = 0; i < this.getPairs().size(); i ++) {
            sb.append(this.getPairs().get(i).toString());
            if (i < this.getPairs().size() - 1) {
                sb.append(", ");
            }
        }
        sb.append("}");
        String result = sb.toString();
        return result;
    }
}