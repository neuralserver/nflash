package org.nflash.runner.node;

public interface Node {
    
    public Node evaluate() throws Exception;
    
    public String toString();
    
}
