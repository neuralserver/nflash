package org.nflash.runner.node;

import org.nflash.runner.processor.UnaryNodeProcessor;
import org.nflash.runner.statement.Statement;


public class UnaryNode extends AbstractNode implements Node {
    
    private Node node;
    private UnaryNodeProcessor proc;
    private Node evaluatedValue;
    
    public UnaryNode(Statement stmnt) {
        super(stmnt);
    }

    public Node evaluate() throws Exception {
        if (this.proc != null) {
            Node result = proc.process(this.getStatement(), this.getNode());
            this.setEvaluatedValue(result);
            return result;
        }
        
        else {
            return this.getNode().evaluate();
        }
    }
    
    public void setNode(Node node) { this.node = node; }
    
    public Node getNode() { return this.node; }

    public void setUnaryNodeProcessor(UnaryNodeProcessor proc) {this.proc = proc; }
    
    public void setEvaluatedValue(Node value) {
        this.evaluatedValue = value;
    }

    public Node getEvaluatedValue() {
        return this.evaluatedValue;
    }

    // FIXME: causes error in one test, check.
    /*
    public String toString() {
        return this.getEvaluatedValue().toString();
    }
    */
}
