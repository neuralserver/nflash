package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.Node;

public class JsonFalse extends JsonValue implements Node {

    public JsonFalse(Statement stmnt) {
        super(stmnt);
    }

    public Node evaluate() throws Exception {
        return this;
    }
    
    public JsonValue getValue() {
        return this;
    }

    @Override
    public String toString() {
        return "false";
    }
}