package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.AbstractNode;
import org.nflash.runner.node.json.JsonValue;
import java.util.List;
import java.util.ArrayList;

public class JsonArray extends JsonValue implements Node {

    private List<Node> values; // Refactor
    
    public JsonArray(Statement stmnt) {
        super(stmnt);
        this.values = new ArrayList<Node>(); // RF
    }

    public void addValue(Node value) {
        this.getValues().add(value);
    }

    public Node evaluate() throws Exception {
        for (Node value : this.getValues()) {
            value.evaluate();
        }
        return this;
    }
    
    public List<Node> getValues() {
        return this.values;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("[");
        
        for (int i = 0; i < this.getValues().size(); i ++) {
            sb.append(this.getValues().get(i).toString());
            if (i < this.getValues().size() - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        String result = sb.toString();
        return result;
    }
}