package org.nflash.runner.node;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.util.JsonUtil;

public class ValueRef extends AbstractNode implements Node {

    private String valueRef;
    private JsonValue evaluatedValue;
    
    public ValueRef(Statement stmnt) {
        super(stmnt);
    }

    public void setValueRef(String valueRef) {
        this.valueRef = valueRef;
    }

    public Node evaluate() throws Exception {
        System.out.println("ENTER ValueRef.evaluate()");
        
        // Value resolution strategy:
        // 1. Check value from local scope
        // 2. Check value from root scope (NflashContext)
        // 3. Check value from let-statements
        
        String stmtId = this.getStatement().getId();
        System.out.println("    >> Check value [" + getValueRef() + "] from stmt: " + stmtId);
        JsonValue value = this.getStatement().getRuntimeValues().get(this.getValueRef()); // getCurrentValue();
        
        // Key didn't exist in current scope, check from rootScope:
        if (value == null) {
            value = this.getStatement().getNflashContext().getRuntimeValues().get(this.getValueRef());
        }
        
        // Key didn't exist, check from letStatements:
        if (value == null) {
            System.out.println("    >> Value not resolved, checking from Let-statements.");
            Statement letStatement = this.getStatement().getNflashContext().getLetStatements().get(this.getValueRef());
            if (letStatement == null) {
                throw new Exception("Value not found for: " + this.getValueRef());
            }
            value = (JsonValue) letStatement.evaluate();
            // Put the evaluated value in the runtimeValues
            this.getStatement().getNflashContext().getRuntimeValues().put(this.getValueRef(), value);
        }
        
        // update the currentValue from the statement
        this.getStatement().setCurrentValue(value);
        this.evaluatedValue = value;
        System.out.println("EXIT ValueRef.evaluate()");
        return value;
    }
    
    public String getValueRef() {
        return this.valueRef;
    }
    
    public String toString() {
        return this.evaluatedValue.toString();
    }
}