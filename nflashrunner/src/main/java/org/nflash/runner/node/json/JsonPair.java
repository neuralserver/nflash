package org.nflash.runner.node.json;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.AbstractNode;

public class JsonPair extends AbstractNode implements Node {

    private String key;
    private JsonValue value;
    private Node evaluatedValue;
    
    public JsonPair(Statement stmnt) {
        super(stmnt);
    }

    public void setPair(String key, JsonValue value ) {
        this.key = key;
        this.value = value;
        this.evaluatedValue = value; // set evaluated value already before evaluating, avoids null-pointer
    }

    public Node evaluate() throws Exception {
        this.evaluatedValue = this.getValue().evaluate();
        return this;
    }
    
    public String getKey() {
        return this.key;
    }
    
    public JsonValue getValue() {
        return this.value;
    }
    
    public Node getEvaluatedValue() {
        return this.evaluatedValue;
    }
    
    public String toString() {
        return this.getKey() + ": " + this.getEvaluatedValue().toString();
    }
}