package org.nflash.runner.node;

import java.util.List;
import java.util.ArrayList;

import org.nflash.runner.statement.Statement;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonTrue;
import org.nflash.runner.node.json.JsonFalse;
import org.nflash.runner.util.JsonUtil;

public class Filter extends AbstractNode implements Node {

    //private Node node;
    private Node filterExpression;
    
    public Filter(Statement stmnt) {
        super(stmnt);
    }

    public void setFilterExpression(Node filterExpression) {
        this.filterExpression = filterExpression;
    }
    
    public Node getFilterExpression() {
        return this.filterExpression;
    }

    public Node evaluate() throws Exception {
        System.out.println("ENTER Filter.evaluate()");
        JsonValue currentValue = this.getStatement().getCurrentValue();
        
        if (currentValue.evaluate() instanceof JsonArray) {
            JsonArray arrayToFilter = (JsonArray) currentValue.evaluate();
            return evaluateArray(arrayToFilter);
        }
        
        else if (currentValue.evaluate() instanceof JsonObj) {
            JsonObj objToFilter = (JsonObj) currentValue.evaluate();
            return evaluateObj(objToFilter);
        }
        
        throw new Exception("Wrong type for filter-expression.");
    }

    private Node evaluateArray(JsonArray arrayToFilter) throws Exception {
        JsonArray arrayResult = new JsonArray(this.getStatement());
        
        for (int i = 0;  i < arrayToFilter.getValues().size(); i ++) {
            Node n = arrayToFilter.getValues().get(i);
            this.getStatement().setCurrentValue((JsonValue) n);
            this.getStatement().getNflashContext().getRuntimeValues().put("@", (JsonValue) n);
            JsonValue evaluated = (JsonValue) this.getFilterExpression().evaluate();
            
            if (evaluated instanceof JsonNumber) {
                int index = ((JsonNumber) evaluated).getNumberValue().intValue();
                // If i+1 == index, then return new JsonArray, with this element.
                if (i + 1 == index) {
                    Node result = arrayToFilter.getValues().get(i);
                    this.getStatement().setCurrentValue( (JsonValue) result );
                    this.getStatement().getNflashContext().getRuntimeValues().put("@", (JsonValue) result);
                    
                    return result;
                }
            }
            
            else if (evaluated instanceof JsonTrue) {
                arrayResult.addValue(arrayToFilter.getValues().get(i));
            }
            
            else if (evaluated instanceof JsonFalse) {
                continue;
            }
            
            else {
                throw new Exception("Array-filter predicate did not evaluate into boolean or number.");
            }
            
        }
        
        return arrayResult;
    }
    
    private Node evaluateObj(JsonObj objToFilter) throws Exception {
        JsonObj objResult = new JsonObj(this.getStatement());

        for (int i = 0;  i < objToFilter.getPairs().size(); i ++) {
            JsonPair jsonPair = objToFilter.getPairs().get(i);
            String key = jsonPair.getKey();
            JsonValue value = jsonPair.getValue();
            JsonObj jsonObj = new JsonObj(this.getStatement());
            JsonPair keyPair = new JsonPair(this.getStatement());
            JsonString keyJson = new JsonString(this.getStatement());
            keyJson.setValue(key);
            keyPair.setPair("\"Key\"", keyJson);
            jsonObj.addPair(keyPair);
            JsonPair valuePair = new JsonPair(this.getStatement());
            valuePair.setPair("\"Value\"", value);
            jsonObj.addPair(valuePair);
            
            this.getStatement().setCurrentValue((JsonValue) jsonObj);
            this.getStatement().getNflashContext().getRuntimeValues().put("@", (JsonObj) jsonObj);
            JsonValue evaluated = (JsonValue) this.getFilterExpression().evaluate();

            if (evaluated instanceof JsonTrue) {
                objResult.addPair(jsonPair);
            }
            
            else if (evaluated instanceof JsonFalse) {
                continue;
            }
            
            else {
                throw new Exception("Obj-filter predicate did not evaluate into boolean.");
            }
        }
        
        return objResult;
    }
}
