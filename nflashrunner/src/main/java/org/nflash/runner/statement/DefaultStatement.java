package org.nflash.runner.statement;

import org.nflash.runner.NflashContext;
import org.nflash.runner.node.Node;

public class DefaultStatement extends BaseStatement implements Statement {

    public DefaultStatement(NflashContext ctx) {
        super(ctx);
    }
    
    public Node evaluate() throws Exception {
        return this.getNode().evaluate();
    }
    
}