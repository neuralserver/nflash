package org.nflash.runner.statement;

import java.util.Map;
import java.util.HashMap;

import org.nflash.runner.NflashContext;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;

public abstract class BaseStatement {
    private String id;
    private Map<String, JsonValue> runtimeValues;

    /**
     * Allow access to root context, allows 
     * e.g. accessing other value-references.
     *
     */
    private NflashContext nfContext;
    
    private Node node;
    
    public JsonValue getCurrentValue() {
        return this.getRuntimeValues().get("@");
    }
    
    public void setCurrentValue(JsonValue currentValue) {
        this.getRuntimeValues().put("@", currentValue);
    }
    
    public Map<String, JsonValue> getRuntimeValues() {
        return runtimeValues;
    }
    
    public NflashContext getNflashContext() {
        return this.nfContext;
    }
    
    public BaseStatement(NflashContext ctx) {
        this.nfContext = ctx;
        this.runtimeValues = new HashMap<String, JsonValue>();
    }
    
    public Node getNode() {
        return this.node;
    }
    
    public void setNode(Node node) {
        this.node = node;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
}