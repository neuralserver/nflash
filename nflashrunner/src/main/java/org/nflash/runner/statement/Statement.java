package org.nflash.runner.statement;

import java.util.Map;

import org.nflash.runner.NflashContext;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;

public interface Statement {

    public Node evaluate() throws Exception;
    
    public void setCurrentValue(JsonValue cv);
    
    public JsonValue getCurrentValue();
    
    public NflashContext getNflashContext();

    public Map<String, JsonValue> getRuntimeValues();
    
    public Node getNode();
    
    public void setNode(Node node);

    public void setId(String id);
    
    public String getId();
}