package org.nflash.runner.statement;

import java.util.List;
import java.util.ArrayList;

import org.nflash.runner.NflashContext;
import org.nflash.runner.node.Node;

public class FunctionStatement extends BaseStatement implements Statement {

    private List<String> arguments; // used to check the argument order and their names
    private List<Statement> letStatements;

    public FunctionStatement(NflashContext ctx) {
        super(ctx);
        this.arguments = new ArrayList<String>();
        this.letStatements = new ArrayList<Statement>();
        this.setId("FunctionStatement");
    }
    
    public Node evaluate() throws Exception {
        return this.getNode().evaluate();
    }
    
    public List<String> getArguments() {
        return this.arguments;
    }
    
    public List<Statement> getLetStatements() {
        return this.letStatements;
    }
    
}