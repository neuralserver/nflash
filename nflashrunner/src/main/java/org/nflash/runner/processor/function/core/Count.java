package org.nflash.runner.processor.function.core;

import org.nflash.runner.NflashContext;

import java.util.List;
import java.util.ArrayList;

import org.nflash.runner.node.AbstractNode;
import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.util.JsonUtil;

public class Count extends AbstractNode implements Node {
    
    public Count(Statement statement) {
        super(statement);
    }

    public Node evaluate() throws Exception {        
        try {
            JsonValue currentValue = this.getStatement().getCurrentValue();
            JsonArray array = (JsonArray) currentValue.evaluate();
            //JsonArray currentValue = (JsonArray) this.getStatement().getCurrentValue();
            
            int resultCount = array.getValues().size();

            JsonValue result = new JsonValue(this.getStatement());
            JsonNumber nn = new JsonNumber(this.getStatement());
            nn.setValue(new Double(resultCount));
            result.setValue(nn);
            this.getStatement().setCurrentValue(result);
            
            return result;
        } catch (Exception e) {
            throw new Exception("Count() :: Invalid input, expected JsonArray");
        }
    }

}