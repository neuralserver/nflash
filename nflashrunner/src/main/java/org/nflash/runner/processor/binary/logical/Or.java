package org.nflash.runner.processor.binary.logical;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonTrue;
import org.nflash.runner.node.json.JsonFalse;
import org.nflash.runner.node.json.JsonNull;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.processor.BaseBinaryNodeProcessor;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public class Or extends BaseBinaryNodeProcessor implements BinaryNodeProcessor {

    @Override
    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        this.preprocess(statement, lhs, rhs);
        
        if (lhsResult instanceof JsonTrue || rhsResult instanceof JsonTrue) {
            return new JsonTrue(statement);
        }

        else {
            return new JsonFalse(statement);
        }
        
    }

}