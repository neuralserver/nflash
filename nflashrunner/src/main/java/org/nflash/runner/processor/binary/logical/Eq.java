package org.nflash.runner.processor.binary.logical;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonTrue;
import org.nflash.runner.node.json.JsonFalse;
import org.nflash.runner.node.json.JsonNull;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.processor.BaseBinaryNodeProcessor;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public class Eq extends BaseBinaryNodeProcessor implements BinaryNodeProcessor {

    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        this.preprocess(statement, lhs, rhs);
        
        if (lhsResult instanceof JsonNumber && rhsResult instanceof JsonNumber) {
        
            System.out.println("=== COMPARING NUMBERS ===");
        
            if ( ((JsonNumber) lhsResult).getNumberValue().equals( ((JsonNumber) rhsResult).getNumberValue()) ) {
                return new JsonTrue(statement);
            }

            return new JsonFalse(statement);
        }
        
        else if (lhsResult instanceof JsonString && rhsResult instanceof JsonString) {
        
            if ( ((JsonString) lhsResult).getStringValue().equals( ((JsonString) rhsResult).getStringValue()) ) {
                return new JsonTrue(statement);
            }

            return new JsonFalse(statement);
        }

        else if (lhsResult instanceof JsonNull && rhsResult instanceof JsonNull) {
        
            if ( ((JsonNull) lhsResult).toString().equals( ((JsonNull) rhsResult).toString()) ) {
                return new JsonTrue(statement);
            }

            return new JsonFalse(statement);
        }
        
        else if (lhsResult instanceof JsonObj && rhsResult instanceof JsonObj) {
        
            if ( ((JsonObj) lhsResult).toString().equals( ((JsonObj) rhsResult).toString()) ) {
                return new JsonTrue(statement);
            }

            return new JsonFalse(statement);
        }

        else if (lhsResult instanceof JsonArray && rhsResult instanceof JsonArray) {
        
            if ( ((JsonArray) lhsResult).toString().equals( ((JsonArray) rhsResult).toString()) ) {
                return new JsonTrue(statement);
            }

            return new JsonFalse(statement);
        }
        
        else {
            // When comparing dynamically calculated result,
            // JsonValue and JsonNumber
            // TODO: if JsonValue, then should compute the correct type first (both: lhs and rhs)
            System.out.println("INCOMPATIBLE TYPES: " + lhsResult.getClass() + ", " + rhsResult.getClass());
            
            // Hack:
            //if (lhsResult.toString().equals(rhsResult.toString())) {
            //    return new JsonTrue(statement);
            //}
            
            return new JsonFalse(statement);
        }
        
    }

}