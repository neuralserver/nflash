package org.nflash.runner.processor.binary;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.processor.BaseBinaryNodeProcessor;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public class Plus extends BaseBinaryNodeProcessor implements BinaryNodeProcessor {

    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        this.preprocess(statement, lhs, rhs);
        
        if (lhsResult instanceof JsonNumber && rhsResult instanceof JsonNumber) {
            JsonNumber result = new JsonNumber(statement);
            result.setValue(((JsonNumber) lhsResult).getNumberValue() + ((JsonNumber) rhsResult).getNumberValue());
            return result;
        }
        
        else if (lhsResult instanceof JsonNumber && rhsResult instanceof JsonString) {
            // TODO: trim the result if source number had no decimals
            // String.valueOf(double) return value with decimals, e.g. "777" -> "777.0"
            JsonString result = new JsonString(statement);
            result.setValue("\"" + String.valueOf(((JsonNumber) lhsResult).toString()) +  ((JsonString) rhsResult).getJavaStringValue() + "\"");
            return result;
        }
        
        else if (lhsResult instanceof JsonString && rhsResult instanceof JsonNumber) {
            // TODO: trim the result if source number had no decimals
            // String.valueOf(double) return value with decimals, e.g. "777" -> "777.0"
            JsonString result = new JsonString(statement);
            result.setValue("\"" + ((JsonString) lhsResult).getJavaStringValue() +  String.valueOf(((JsonNumber) rhsResult).toString()) + "\"");
            return result;
        }
        
        else {
            JsonString result = new JsonString(statement);
            result.setValue("\"" + ((JsonString) lhsResult).getJavaStringValue() + ((JsonString) rhsResult).getJavaStringValue() + "\"");
            return result;
        }
        
    }

}