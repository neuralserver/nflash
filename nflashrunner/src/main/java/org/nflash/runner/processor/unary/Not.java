package org.nflash.runner.processor.unary;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonTrue;
import org.nflash.runner.node.json.JsonFalse;
import org.nflash.runner.processor.UnaryNodeProcessor;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public class Not implements UnaryNodeProcessor {

    public Node process(Statement statement, Node node) throws Exception {
        Node nodeResult = node.evaluate();
        
        if (nodeResult instanceof JsonTrue) {
            return new JsonFalse(statement);
        }
        
        else if (nodeResult instanceof JsonFalse) {
            return new JsonTrue(statement);
        }
        
        else {
            throw new Exception("Not a boolean value.");
        }
        
    }

}