package org.nflash.runner.processor;

import org.nflash.runner.node.Node;
import org.nflash.runner.statement.Statement;

/**
 * 
 * Processor interface for unary nodes
 * 
 */
public interface UnaryNodeProcessor {

    public Node process(Statement statement, Node node) throws Exception;

}