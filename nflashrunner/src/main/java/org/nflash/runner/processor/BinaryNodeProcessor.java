package org.nflash.runner.processor;

import org.nflash.runner.node.Node;
import org.nflash.runner.statement.Statement;

/**
 * 
 * Processor interface for binary nodes
 * 
 */
public interface BinaryNodeProcessor {

    public Node process(Statement statement, Node lhs, Node rhs) throws Exception;

}