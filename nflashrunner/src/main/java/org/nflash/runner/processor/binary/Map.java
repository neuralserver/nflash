package org.nflash.runner.processor.binary;

import java.util.List;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.util.JsonUtil;

/**
 * 
 * 
 * 
 */
public class Map implements BinaryNodeProcessor {

    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        Node lhsResult = lhs.evaluate();
        
        // The rhs is evaluated against the lhs, for each element in an array.
        if (lhsResult instanceof JsonArray) {
            JsonArray jsonArray = (JsonArray) lhsResult;
            List<Node> arrayValues = jsonArray.getValues();
            JsonArray resultArray = new JsonArray(statement);
            for (int i = 0; i < arrayValues.size(); i ++) {
                Node arrayValue = arrayValues.get(i);
                statement.getNflashContext().getRuntimeValues().put("@", (JsonValue) arrayValue);
                statement.setCurrentValue((JsonValue) arrayValue);
                Node rhsResult = rhs.evaluate();
                resultArray.addValue(((JsonValue) rhsResult).copy()); // Add the deep-copy instead of the value-reference to prevent modifation from memory.
            }
            statement.getNflashContext().getRuntimeValues().put("@", (JsonValue) resultArray);
            statement.setCurrentValue((JsonValue) resultArray);
            return resultArray;
        }
        
        else {
            throw new Exception("Unsupported data-type to map against.");
        }

    }

}