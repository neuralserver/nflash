package org.nflash.runner.processor;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public abstract class BaseBinaryNodeProcessor implements BinaryNodeProcessor {

    protected Node lhsResult;
    protected Node rhsResult;

    public void preprocess(Statement statement, Node lhs, Node rhs) throws Exception {
        JsonValue initialValue = statement.getCurrentValue();
        // @ might be null when executing only select statement (from unit-tests)
        if (initialValue != null) {
            initialValue = initialValue.copy();
        }
        
        lhsResult = lhs.evaluate();
        
        // Fork the initial value for rhs:
        statement.setCurrentValue(initialValue);

        rhsResult = rhs.evaluate();
        
        if (lhsResult instanceof JsonValue) {
            lhsResult = lhsResult.evaluate();
        }
        
        if (rhsResult instanceof JsonValue) {
            rhsResult = rhsResult.evaluate();
        }
    }
    
    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        return null;
    }

}