package org.nflash.runner.processor.binary;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.processor.BaseBinaryNodeProcessor;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.statement.Statement;

/**
 * 
 * 
 * 
 */
public class Modulus extends BaseBinaryNodeProcessor implements BinaryNodeProcessor {

    public Node process(Statement statement, Node lhs, Node rhs) throws Exception {
        this.preprocess(statement, lhs, rhs);
        
        if (lhsResult instanceof JsonNumber && rhsResult instanceof JsonNumber) {
            JsonNumber result = new JsonNumber(statement);
            result.setValue(((JsonNumber) lhsResult).getNumberValue() % ((JsonNumber) rhsResult).getNumberValue());
            return result;
        } else {
            throw new Exception("Not a number.");
        }
        
    }

}