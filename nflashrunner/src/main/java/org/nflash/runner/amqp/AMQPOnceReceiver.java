package org.nflash.runner.amqp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

//
// Adapted from https://github.com/apache/qpid-jms/blob/master/qpid-jms-examples/src/main/java/org/apache/qpid/jms/example/HelloWorld.java
//
public class AMQPOnceReceiver {
    private static final String USER = System.getenv("ARTEMIS_USERNAME"); // "ADMIN";
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD"); // "ADMIN";
    
    public void AMQPReceiver() { }
    
    public Message receiveSync(String queueName, String correlationId, int timeout) throws Exception {
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        MessageConsumer messageConsumer = session.createConsumer(queue, "JMSCorrelationID='" + correlationId + "'");
        System.out.println("Connected. Starting to listen queue :: " + queueName);
        
        Message message = messageConsumer.receive(timeout);
        if (message == null) {
            throw new Exception("Did not receive reply from neural model. TODO: restart query(?)");
        }
        System.out.println("Once receiver, received message :: " + message.toString());
        connection.close();
        return message;
    }
    
    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}