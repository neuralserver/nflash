package org.nflash.runner.amqp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.nflash.runner.model.NeuralModelCallRequest;

public class AMQPSender {
    private static final int DEFAULT_COUNT = 10;
    private static final int DELIVERY_MODE = DeliveryMode.NON_PERSISTENT;
    private static final String USER = System.getenv("ARTEMIS_USERNAME");
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD");

    public AMQPSender() { }

    // 
    // This method is used to send message to neural models via context-server.
    // 
    public void sendToContextServer(String queueName, String replyQueueName, 
                    NeuralModelCallRequest nmCallRequest, 
                    String correlationId, String transactionId) throws Exception {
        // The configuration for the Qpid InitialContextFactory has been supplied in
        // a jndi.properties file in the classpath, which results in it being picked
        // up automatically by the InitialContext constructor.
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        Destination replyQueue = session.createQueue(replyQueueName);

        MessageProducer messageProducer = session.createProducer(queue);
        long messageTimeToLive = 60000;

        System.out.println("[ RUNNER :: NM CALL ]==================");
        System.out.println("Json: " + nmCallRequest.toJsonString());
        System.out.println("=======================================");
        TextMessage message = session.createTextMessage(nmCallRequest.toJsonString());
        
        //
        // This tells the context-server to handle TEXT-type of request, i.e. pull the bytebuffer from the memory
        // and apply optionally defined bbox-clipping.
        //
        message.setJMSType("TEXT");
        message.setJMSCorrelationID(correlationId);
        message.setJMSReplyTo(replyQueue);
        messageProducer.send(message, DELIVERY_MODE, Message.DEFAULT_PRIORITY, messageTimeToLive);

        connection.close();
    }

    public void sendToQueue(String queueName, String messageToSend) throws Exception {
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);

        MessageProducer messageProducer = session.createProducer(queue);

        TextMessage message = session.createTextMessage(messageToSend);
        messageProducer.send(message);

        connection.close();
    }

    public void sendToTopic(String topicName, String messageToSend) throws Exception {
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination topic = session.createTopic(topicName);

        MessageProducer messageProducer = session.createProducer(topic);

        TextMessage message = session.createTextMessage(messageToSend);
        messageProducer.send(message);

        connection.close();
    }

    public void sendToQueueCorrelated(String queueName, String messageToSend, String correlationId) throws Exception {
        Context context = new InitialContext();
        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);

        MessageProducer messageProducer = session.createProducer(queue);
        long messageTimeToLive = 180000;

        TextMessage message = session.createTextMessage(messageToSend);
        message.setJMSCorrelationID(correlationId);
        messageProducer.send(message); // TODO: set ttl

        connection.close();
    }

    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("AMQPSender :: Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}