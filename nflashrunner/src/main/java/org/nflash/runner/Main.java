package org.nflash.runner;

import java.io.File;

public class Main {
    public static void main(String [] args) throws Exception {
        if (args[0] == null) {
            System.out.println("Please give the file name of the query.");
            System.exit(1);
        }

        String fileName = args[0];
        File file = new File(fileName);
        
        NflashRunner runner = new NflashRunner();
        //runner.setFileInputStream(file);
        runner.setQuery("TODO: set from file");
        Thread q1 = new Thread(runner, "Default Query");
        q1.start();
        //q1.stop();
    }
}