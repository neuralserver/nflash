package org.nflash.runner;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;

import org.nflash.parser.*;
import org.nflash.runner.processor.function.core.Count;
import org.nflash.runner.processor.UnaryNodeProcessor;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.unary.*;
import org.nflash.runner.processor.binary.*;
import org.nflash.runner.processor.binary.logical.*;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.statement.FunctionStatement;
import org.nflash.runner.node.*;
import org.nflash.runner.node.json.*;
import org.nflash.runner.util.JsonUtil;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import java.util.Collections;
import java.io.IOException;

/**
 * This class listens the events that parser emits,
 * and reacts by wiring the required classes into 
 * the NflashContext.
 *
 */
public class NflashCompiler extends NflashBaseListener {

    private NflashContext nfContext;
    private Statement currentStatement;
    private Stack<Node> stack;
    
    public NflashCompiler() {
        super();
        this.stack = new Stack();
    }
    
    public void setNfContext(NflashContext nfContext) {
        this.nfContext = nfContext;
    }

    public NflashContext getNfContext() {
        return this.nfContext;
    }
    
    public void setCurrentStatement(Statement stmnt) {
        this.currentStatement = stmnt;
    }
    
    public Statement getCurrentStatement() {
        return this.currentStatement;
    }
    
    @Override
    public void enterNf(NflashParser.NfContext ctx) {
        System.out.println("Starting to compile. ");
    }

    @Override
    public void exitNf(NflashParser.NfContext ctx) {
        System.out.println("====== COMPILING DONE ======");
    }

    @Override
    public void enterFrom(NflashParser.FromContext ctx) {
        System.out.println("Configure input device connection. ");
    }

    @Override
    public void exitFrom(NflashParser.FromContext ctx) {
        System.out.println("Input device connection configuration done.");
        // TODO: add input device to neural application context
    }

    @Override
    public void enterInput_source_name(NflashParser.Input_source_nameContext ctx) {
        System.out.println("Input source: " + ctx.ID() + ". ");
        nfContext.getInputSource().setName(ctx.ID().toString());
    }

    @Override
    public void enterInput_source_alias(NflashParser.Input_source_aliasContext ctx) {
        System.out.println("Input source bound to: " + ctx.CONST_ID() + ". ");
        // When in _From_ -statement's alias binding, then assign inputSourceBinding = ctx.CONST_ID()
        // First check which rule we are into.
        if (ctx.getRuleIndex() == NflashParser.RULE_input_source_alias) {
            // System.out.println("Parent rule index: " + ctx.getParent().getRuleIndex());
            if (ctx.getParent().getRuleIndex() == NflashParser.RULE_from) {
                nfContext.getInputSource().setBindingVariable(ctx.CONST_ID().toString());
            }
        }
    }
    
    @Override
    public void enterSelect(NflashParser.SelectContext ctx) {
        Statement select = new DefaultStatement(nfContext);
        select.setId("SelectStatement");
        this.setCurrentStatement(select);
    }

    @Override
    public void exitSelect(NflashParser.SelectContext ctx) {
        System.out.println("EXIT Select :: Stack size :: " + this.stack.size());
        this.currentStatement.setNode(this.stack.pop());
        nfContext.setSelectStatement(this.currentStatement);
    }

    @Override
    public void enterFunction_stmt(NflashParser.Function_stmtContext ctx) {
        Statement functionStatement = new FunctionStatement(nfContext);
        this.setCurrentStatement(functionStatement);
    }

    @Override
    public void exitFunction_stmt(NflashParser.Function_stmtContext ctx) {
        System.out.println("EXIT Function-stmt :: Stack size :: " + this.stack.size());
        this.currentStatement.setNode(this.stack.pop()); // Function Expression
        List<ParseTree> subNodes = getContextChildNodes(ctx);
        int subNodesSize = subNodes.size();
        System.out.println("  SubNodes :: " + subNodesSize);
        
        String functionNamespace = "";
        String functionName = "";
        
        List<String> functionArguments = new ArrayList<String>();
        
        // Function namespace used:
        if (subNodes.get(2).getText().equals(":")) {
            functionNamespace = subNodes.get(1).getText();
            functionName = subNodes.get(3).getText();
        }
        
        //
        // Work without namespace first:
        //
        else {
            functionName = subNodes.get(1).getText();
            
            // Collect arguments:
            int argumentsEndParenthesesIndex = 0;
            for (int i = 3; i < subNodesSize - 1; i ++) {
                
                // Reach end of arguments
                if (subNodes.get(i) instanceof TerminalNode && subNodes.get(i).getText().equals(")")) {
                    argumentsEndParenthesesIndex = i;
                    break;
                }
                
                // Skip argument separator
                else if (subNodes.get(i) instanceof TerminalNode && subNodes.get(i).getText().equals(",")) {
                    continue;
                }
                
                // Add argument
                else {
                    String functionArgument = subNodes.get(i).getText();
                    functionArguments.add(functionArgument);
                }
            }
            ((FunctionStatement) this.currentStatement).getArguments().addAll(functionArguments);
            
            // Collect Let-statements: SKIP FIRST
            List<Statement> letStatements = new ArrayList<Statement>();
            for (int i = subNodesSize - 1; i > argumentsEndParenthesesIndex; i --) {
                
                if (subNodes.get(i) instanceof RuleNode && subNodes.get(i) instanceof NflashParser.Let_stmtContext) {
                    System.out.println("    >> LET STMT FOUND");
                    // letStatements.add(this.stack.pop());
                }
                
            }
        }
        // Add functionStatement into NflashContext
        nfContext.getFunctionStatements().put(functionNamespace + ":" + functionName + ":" + functionArguments.size(), (FunctionStatement) this.currentStatement);
    }
    
    @Override
    public void enterLet_stmt(NflashParser.Let_stmtContext ctx) {
        Statement letStatement = new DefaultStatement(nfContext);
        letStatement.setId("LetStatement");
        this.setCurrentStatement(letStatement);
    }
    
    @Override
    public void exitLet_stmt(NflashParser.Let_stmtContext ctx) {
        System.out.println("EXIT Let :: Stack size :: " + this.stack.size());
        this.currentStatement.setNode(this.stack.pop());
        String constId = ctx.CONST_ID().getSymbol().getText();
        nfContext.getLetStatements().put(constId, this.currentStatement);
    }
    
    @Override
    public void exitCondition(NflashParser.ConditionContext ctx) {
        System.out.println("EXIT When :: Stack size :: " + this.stack.size());
        List<ParseTree> subNodes = getContextChildNodes(ctx);
        int subNodesSize = subNodes.size();
        System.out.println("  SubNodes :: " + subNodesSize);
        Condition condition = new Condition(this.currentStatement);
        Node otherwise = this.stack.pop();
        condition.setOtherwise(otherwise);
        
        while (this.stack.size() > 0) {
            Node then = this.stack.pop();
            Node when = this.stack.pop();
            condition.addWhen(when);
            condition.addThen(then);
        }
        
        this.stack.push(condition);
    }
    
    @Override
    public void exitFilter_expr(NflashParser.Filter_exprContext ctx) {
        System.out.println("EXIT Filter :: Stack size :: " + this.stack.size());
        Filter filter = new Filter(this.currentStatement);
        Node filterExpression = this.stack.pop();
        filter.setFilterExpression(filterExpression);
        this.stack.push(filter);
    }
    
    /**
     * When exiting an Expr, then pop the stack, and wire new Node (Unary, Binary, or Multi).
     * 
     */
    @Override public void exitExpr(NflashParser.ExprContext ctx) {
        System.out.println("EXIT Expr :: Stack size :: " + this.stack.size());
        List<ParseTree> subNodes = getContextChildNodes(ctx);
        int subNodesSize = subNodes.size();
        
        if (subNodesSize == 1) {
            System.out.println("ENTER UNARYNODE :: SubNodesSize == 1, Stack size == " + this.stack.size());
            UnaryNode unode = new UnaryNode(this.currentStatement);
            unode.setNode(this.stack.pop());
            this.stack.push(unode);
            System.out.println("EXIT SubNodesSize == 1");
        }
        
        else if (subNodesSize == 3 && subNodes.get(1) instanceof TerminalNode) {
            System.out.println("ENTER BINARYNODE");
            Node rhs = this.stack.pop();
            Node lhs = this.stack.pop();
            BinaryNodeProcessor op = null;
            
            // Check which op and create and assign it
            TerminalNode token = ctx.getToken(NflashParser.PLUS, 0);
            
            if (token != null) {
                op = new Plus();
                System.out.println("  Plus()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.MINUS, 0)) != null) {
                op = new Minus();
                System.out.println("  Minus()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.MULT, 0)) != null) {
                op = new Multiplicate();
                System.out.println("  Multiplicate()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.DIV, 0)) != null) {
                op = new Division();
                System.out.println("  Division()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.MOD, 0)) != null) {
                op = new Modulus();
                System.out.println("  Modulus()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.POW, 0)) != null) {
                op = new Power();
                System.out.println("  Power()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.EQ, 0)) != null) {
                op = new Eq();
                System.out.println("  Eq()");
            }

            else if ( (token = ctx.getToken(NflashParser.GT, 0)) != null) {
                op = new Gt();
                System.out.println("  Gt()");
            }

            else if ( (token = ctx.getToken(NflashParser.GTE, 0)) != null) {
                op = new Gte();
                System.out.println("  Gte()");
            }

            else if ( (token = ctx.getToken(NflashParser.LT, 0)) != null) {
                op = new Lt();
                System.out.println("  Lt()");
            }

            else if ( (token = ctx.getToken(NflashParser.LTE, 0)) != null) {
                op = new Lte();
                System.out.println("  Lte()");
            }

            else if ( (token = ctx.getToken(NflashParser.AND, 0)) != null) {
                op = new And();
                System.out.println("  And()");
            }

            else if ( (token = ctx.getToken(NflashParser.OR, 0)) != null) {
                op = new Or();
                System.out.println("  Or()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.MAP, 0)) != null) {
                op = new Map();
                System.out.println("  Map()");
            }
            
            BinaryNode bnode = new BinaryNode(this.currentStatement);
            bnode.setLhs(lhs);
            bnode.setRhs(rhs);
            bnode.setBinaryNodeProcessor(op);
            this.stack.push(bnode);
            System.out.println("EXIT BINARYNODE");
        }
        
        else if (subNodesSize == 2 
                && subNodes.get(0) instanceof TerminalNode) {
            System.out.println("ENTER UnaryNode (- or Not)");

            UnaryNode unode = new UnaryNode(this.currentStatement);
            unode.setNode(this.stack.pop());

            UnaryNodeProcessor op = null;
            
            // Check which op and create and assign it
            TerminalNode token = ctx.getToken(NflashParser.NOT, 0);
            
            if (token != null) {
                op = new Not();
                System.out.println("  Not()");
            }
            
            else if ( (token = ctx.getToken(NflashParser.MINUS, 0)) != null) {
                op = new Negate();
                System.out.println("  Negate()");
            }

            unode.setUnaryNodeProcessor(op);
            this.stack.push(unode);
            System.out.println("EXIT SubNodesSize == 1");
        }
        
        else if (subNodesSize == 3 
                && subNodes.get(0) instanceof TerminalNode
                && subNodes.get(2) instanceof TerminalNode) {
            System.out.println("ENTER UnaryNode (Parentheses)");

            UnaryNode unode = new UnaryNode(this.currentStatement);
            unode.setNode(this.stack.pop());
            this.stack.push(unode);
            System.out.println("EXIT SubNodesSize == 1");
        }
        
        else {
            System.out.println("ENTER MultiNode");
            MultiNode mnode = new MultiNode(this.currentStatement);
            System.out.println("  MultiNode :: Child count :: " + subNodesSize);
            System.out.println("  MultiNode :: Stack size :: " + this.stack.size());
            
            // Check if parentheses (pos 0, and 2): "'(' expr ')' filter_expr | obj_access"
            if (subNodes.get(0) instanceof TerminalNode && subNodes.get(2) instanceof TerminalNode
                && subNodes.get(1) instanceof RuleNode && subNodes.get(3) instanceof RuleNode) {
                System.out.println("    >> Parentheses");
                mnode.addNode(this.stack.pop()); // Note that multiNode evaluates in reverse-order (corrects order)
                mnode.addNode(this.stack.pop());
                System.out.println("  MultiNode :: Stack size :: " + this.stack.size());
            }
            
            else {
                for (int i = 0; i < subNodesSize; i ++) {
                    Node node = this.stack.pop();
                    //System.out.println("    Pop stack.");
                    mnode.addNode(node);
                    //System.out.println("    Add node.");
                }
            }

            this.stack.push(mnode);
            //System.out.println("Push mnode.");
            System.out.println("EXIT MultiNode");
        }
    }

    @Override
    public void exitValue_ref(NflashParser.Value_refContext ctx) {
        System.out.println("EXIT Value_ref :: Stack size :: " + this.stack.size());
        ValueRef vrNode = new ValueRef(this.currentStatement);
        
        // Set correct symbol:
        String symbol = null;
        
        if (ctx.CONST_ID() != null) {
            symbol = ctx.CONST_ID().toString();
        }
        
        else if (ctx.CURRENT_VALUE() != null) {
            symbol = ctx.CURRENT_VALUE().toString();
        }
        
        else if (ctx.DATAFRAME_VALUE() != null) {
            symbol = ctx.DATAFRAME_VALUE().toString();
        }
        vrNode.setValueRef(symbol);
        this.stack.push(vrNode);
    }

    @Override
    public void exitJson(NflashParser.JsonContext ctx) {
        System.out.println("EXIT Json :: Stack size :: " + this.stack.size());
        Node n = this.stack.peek();
        if (n == null) {System.out.println("NULL VALUE FROM STACK!!!");} // TODO: remove this, only used while debugging
        //System.out.println(n.toString());
        this.currentStatement.setNode(n);
    }
    
    @Override
    public void exitJson_value(NflashParser.Json_valueContext ctx) {
        System.out.println("EXIT Json_value :: Stack size :: " + this.stack.size());
        JsonValue jsonValue = new JsonValue(this.currentStatement);
        List<ParseTree> subNodes = this.getContextChildNodes(ctx);
        
        System.out.println("CHILD NODES :: " + subNodes.size() + " :: CHILD NODE [0] TYPE :: " + subNodes.get(0).getClass().getName());
        
        if (subNodes.size() > 0 
            && subNodes.get(0) instanceof RuleNode 
            && subNodes.get(0) instanceof NflashParser.Json_objContext) {
            
            jsonValue.setValue(this.stack.pop()); // set JsonObj from stack
        }

        else if (subNodes.size() > 0 
            && subNodes.get(0) instanceof RuleNode 
            && subNodes.get(0) instanceof NflashParser.Json_arrayContext) {
            System.out.println("SETTING JSON VALUE --> JSON ARRAY.");
            Node value = this.stack.pop();
            if (value == null) {
                System.out.println("WARNING:: POPPED NULL VALUE!!!");
            }
            jsonValue.setValue(value); // set JsonArray from stack   
        }

        else if (subNodes.size() > 0 && subNodes.get(0) instanceof TerminalNode) {
            TerminalNode token = ctx.getToken(NflashParser.STRING, 0);
            
            if (token != null) {
                JsonString sNode = new JsonString(this.currentStatement);
                String symbolText = token.getSymbol().getText();
                System.out.println("TerminalNode. Text :: " + symbolText);
                sNode.setValue(symbolText);
                jsonValue.setValue(sNode);
            }
            
            else if ( (token = ctx.getToken(NflashParser.NUMBER, 0)) != null) {
                JsonNumber nNode = new JsonNumber(this.currentStatement);
                String symbolText = token.getSymbol().getText();
                System.out.println("TerminalNode. Text :: " + symbolText);
                nNode.setValue(Double.valueOf(symbolText));
                jsonValue.setValue(nNode);
            }
            
            else if ( (token = ctx.getToken(NflashParser.JSON_FALSE, 0)) != null) {
                JsonFalse jsonFalse = new JsonFalse(this.currentStatement);
                jsonValue.setValue(jsonFalse);
            }
            
            else if ( (token = ctx.getToken(NflashParser.JSON_TRUE, 0)) != null) {
                JsonTrue jsonTrue = new JsonTrue(this.currentStatement);
                jsonValue.setValue(jsonTrue);
            }
            
            else if ( (token = ctx.getToken(NflashParser.JSON_NULL, 0)) != null) {
                JsonNull jsonNull = new JsonNull(this.currentStatement);
                jsonValue.setValue(jsonNull);
            }

        }
        
        this.stack.push(jsonValue);
    }
    
    @Override
    public void exitJson_array(NflashParser.Json_arrayContext ctx) {
        System.out.println("EXIT Json_array :: Stack size :: " + this.stack.size());
        List<ParseTree> ruleChildNodes = this.getContextChildRuleNodes(ctx);
        int childCount = ruleChildNodes.size();
        
        JsonArray jsonArray = new JsonArray(this.currentStatement);
        
        // For reversing traversed nodes
        List<Node> jsonValues = new ArrayList<Node>(); // Refactor: JsonValue -> Node
        
        for (int i = 0; i < childCount; i ++) {
            Node jsonValue = (Node) this.stack.pop();
            jsonValues.add(jsonValue);
        }
        
        // For reversing traversed nodes
        for (int i = jsonValues.size() - 1; i >= 0; i --) {
            jsonArray.addValue(jsonValues.get(i));
        }
        
        this.stack.push(jsonArray);
    }
    
    @Override
    public void exitJson_obj(NflashParser.Json_objContext ctx) {
        System.out.println("EXIT Json_obj :: Stack size :: " + this.stack.size());
        List<ParseTree> ruleChildNodes = this.getContextChildRuleNodes(ctx);
        int pairsCount = ruleChildNodes.size();
        
        JsonObj jsonObj = new JsonObj(this.currentStatement);
        
        List<JsonPair> jsonPairs = new ArrayList<JsonPair>();
        
        for (int i = 0; i < pairsCount; i ++) {
            JsonPair jsonPair = (JsonPair) this.stack.pop();
            jsonPairs.add(jsonPair);
        }
        
        for (int i = jsonPairs.size() - 1; i >= 0; i --) {
            jsonObj.addPair(jsonPairs.get(i));
        }
        
        this.stack.push(jsonObj);
    }
    
    @Override
    public void exitJson_pair(NflashParser.Json_pairContext ctx) {
        System.out.println("EXIT Json_pair :: Stack size :: " + this.stack.size());
        JsonPair jsonPair = new JsonPair(this.currentStatement);
        String key = ctx.STRING().getSymbol().getText();
        JsonValue value = new JsonValue(this.currentStatement);
        value.setValue(this.stack.pop());
        jsonPair.setPair(key, value);
        this.stack.push(jsonPair);
    }
    
    @Override
    public void exitObj_access(NflashParser.Obj_accessContext ctx) {
        System.out.println("EXIT OBJ_ACCESS :: " + ctx.ID());
        ObjAccess objAccess = new ObjAccess(this.currentStatement);
        objAccess.setObjAccessKey(ctx.ID().getSymbol().getText());
        this.stack.push(objAccess);
    }
    
    @Override
    public void exitNeural_model_call(NflashParser.Neural_model_callContext ctx) {
        System.out.println("EXIT Neural_model_call :: Stack size :: " + this.stack.size());
        List<ParseTree> nodes = getContextChildNodes(ctx);

        NeuralModelCall nmCall = new NeuralModelCall(this.currentStatement);

        if (nodes.get(1).getClass().getName().equals("org.nflash.parser.NflashParser$Neural_model_nameContext")) {
            String nmCallModelName = ((NflashParser.Neural_model_nameContext)nodes.get(1)).NEURAL_MODEL_ID().toString();
            System.out.println("COMPILER :: NM Call Model :: " + nmCallModelName);
            nmCall.setRhsTargetModel(nmCallModelName);
        }
        
        System.out.println("COMPILER :: NM CALL :: NODES LEN :: " + nodes.size());
        
        if (nodes.size() > 2 && nodes.get(2).getClass().getName().equals("org.nflash.parser.NflashParser$Json_objContext")) {
            String jsonConfigValue = ((NflashParser.Json_objContext)nodes.get(2)).getText().toString();
            System.out.println("COMPILER :: Adding nm json-config to nmCall :: " + jsonConfigValue);
            nmCall.setJsonConfiguration(JsonUtil.jsonValueFromString(jsonConfigValue));
        }
        this.stack.push(nmCall);

    }

    @Override public void exitFunction_call(NflashParser.Function_callContext ctx) {
        List<ParseTree> subNodes = getContextChildNodes(ctx);
        int subNodesSize = subNodes.size();
        System.out.println("FUNCTION :: SUBNODES :: " + subNodesSize + ". " + subNodes.get(0).getText());
        
        String functionNamespace = "";
        String functionName = "";
        
        // Function namespace used:
        if (subNodes.get(2).getText().equals(":")) {
            functionNamespace = subNodes.get(1).getText();
            functionName = subNodes.get(3).getText();
        }
        
        else {
            functionName = subNodes.get(1).getText();
        }
        
        System.out.println("    >> FUNCTION :: " + functionName + ". Namespace: " + functionNamespace);

        // Collect params:
        List<Node> functionParams = new ArrayList<Node>();
        int startPos = 5;
        if (functionNamespace.equals("")) {
            startPos = 3;
        }
        int paramsEndParenthesesIndex = 0;
        for (int i = startPos; i < subNodesSize - 1; i ++) {
            
            // Reach end of params
            if (subNodes.get(i) instanceof TerminalNode && subNodes.get(i).getText().equals(")")) {
                paramsEndParenthesesIndex = i;
                break;
            }
            
            // Skip argument separator
            else if (subNodes.get(i) instanceof TerminalNode && subNodes.get(i).getText().equals(",")) {
                continue;
            }
            
            // Add params
            else {
                Node functionParam = this.stack.pop();
                functionParams.add(functionParam);
            }
        }
        
        // Check if one of core-functions:
        // TODO: create a proper resolver
        if (functionNamespace.equals("array") || functionName.equals("Count")) {
            Node function = null;
            if (functionName.equals("Count")) {
                function = new Count(this.currentStatement);
                this.stack.push(function);
            }
        }

        // User-defined function:
        else {
            FunctionCall fnCall = new FunctionCall(this.currentStatement);
            fnCall.setFunctionFQName(functionNamespace + ":" + functionName + ":" + functionParams.size());
            Collections.reverse(Arrays.asList(functionParams));
            fnCall.getParams().addAll(functionParams);
            this.stack.push(fnCall);
        }
    }

    //
    // Helper functions
    //
    
    public List<ParseTree> getContextChildNodes(ParserRuleContext context) {
        List<ParseTree> nodes = new ArrayList<ParseTree>();
        for (int i = 0; i < context.getChildCount(); i++) {
            nodes.add(context.getChild(i));
        }
        return nodes;
    }

    public List<ParseTree> getContextChildRuleNodes(ParserRuleContext context) {
        List<ParseTree> nodes = new ArrayList<ParseTree>();
        for (int i = 0; i < context.getChildCount(); i++) {
            if (context.getChild(i) instanceof RuleNode) {
                nodes.add(context.getChild(i));
            }
        }
        return nodes;
    }
    
    public List<ParseTree> getParseTreeChildRuleNodes(ParseTree pt) {
        List<ParseTree> nodes = new ArrayList<ParseTree>();
        for (int i = 0; i < pt.getChildCount(); i++) {
            if (pt.getChild(i) instanceof RuleNode) {
                nodes.add(pt.getChild(i));
            }
        }
        return nodes;
    }

}
