package org.nflash.runner;

// import ANTLR's runtime libraries
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.nflash.parser.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class NflashRunner implements Runnable {
    private InputStream is;
    private InputStream getIs() { return this.is; }
    private NflashCompiler compiler;
    private NflashContext nfContext;
    private String query;
    private String queryId;
    
    public void run() {
        try {
            NflashContext nfContext = new NflashContext();
            this.compile(nfContext);
            System.out.print("Starting to run query.");
            this.startContext(nfContext); // --> start reading frames from input device and pass them to Select -statement processor
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public NflashContext compile(NflashContext nfContext) throws IOException {
        this.is = new ByteArrayInputStream(this.query.getBytes(StandardCharsets.UTF_8));
        
        // Create a CharStream that reads from standard input
        ANTLRInputStream input = new ANTLRInputStream(getIs());

        // create a lexer that feeds off of input CharStream
        NflashLexer lexer = new NflashLexer(input);
        
        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        
        // create a parser that feeds off the tokens buffer
        NflashParser parser = new NflashParser(tokens);
        ParseTree tree = parser.nf(); // begin parsing at init rule
        
        // Create a generic parse tree walker that can trigger callbacks
        ParseTreeWalker walker = new ParseTreeWalker();
        
        // Walk the tree created during the parse, trigger callbacks
        compiler = new NflashCompiler();
        nfContext.setContextId(getQueryId());
        compiler.setNfContext(nfContext);
        walker.walk(compiler, tree);
        return nfContext;
    }
    
    private void startContext(NflashContext ctx) {
        ctx.start();
    }
    
    public void shutdown() {
        getNfContext().shutdown();
    }
    
    public NflashContext getNfContext() {
        return this.nfContext;
    }

    public void setQueryId(String id) {
        this.queryId = id;
    }
    
    public String getQueryId() {
        return this.queryId;
    }
    
    public void setQuery(String query) {
        this.query = query;
    }
    
    public String getQuery() {
        return this.query;
    }
}