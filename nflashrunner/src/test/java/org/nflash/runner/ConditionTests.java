package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class ConditionTests {

    @Test
    public void conditionTest() throws Exception {
        String query = "From Vision.input_source As $is Select When 1 = 1 Then \"foo\" Otherwise \"bar\"";
        assertEquals("\"foo\"", doQuery(query).toString());
    }

    @Test
    public void condition2Test() throws Exception {
        String query = "From Vision.input_source As $is Select When 1 = 0 Then \"foo\" Otherwise \"bar\"";
        assertEquals("\"bar\"", doQuery(query).toString());
    }
    
    @Test
    public void condition3Test() throws Exception {
        String query = "From Vision.input_source As $is Select When 1 = 0 Then \"foo\" When true Then 1 + 2 Otherwise \"bar\"";
        assertEquals("3.0", doQuery(query).toString());
    }
    
    @Test
    public void condition4Test() throws Exception {
        String query = "From Vision.input_source As $is Let $foo := When [1,2,3] => Count() > 2 Then \"trebar\" Otherwise \"fubar\" Select $foo";
        assertEquals("\"trebar\"", doQuery(query).toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}