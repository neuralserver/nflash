package org.nflash.runner.node.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.Map;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.ValueRef;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.util.JsonUtil;

public class JsonUtilTests {

    @Test
    public void CompileJsonValueObjTest() throws Exception {
        String json = "{\"foo\": \"777\", \"bin\": \"bai\"}";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("{\"foo\": \"777\", \"bin\": \"bai\"}", jsonValue.toString());
    }

    @Test
    public void CompileJsonValueObjTest2() throws Exception {
        String json = "{}";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("{}", jsonValue.toString());
    }

    @Test
    public void CompileJsonValueStringTest() throws Exception {
        String json = "\"foo\"";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("\"foo\"", jsonValue.toString());
    }

    @Test
    public void CompileJsonValueArrayTest() throws Exception {
        String json = "[]";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("[]", jsonValue.toString());
    }

    @Test
    public void CompileJsonValue3Test() throws Exception {
        String json = "[\"foo\"]";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("[\"foo\"]", jsonValue.toString());
    }
    
    @Test
    public void CompileJsonValue4Test() throws Exception {
        String json = "[\"foo\", 777]";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("[\"foo\", 777.0]", jsonValue.toString());
    }
    
    @Test
    public void CompileJsonValue5Test() throws Exception {
        String json = "[\"foo\", 777, {\"bin\": 13}]";
        JsonValue jsonValue = JsonUtil.jsonValueFromString(json);
        assertEquals("[\"foo\", 777.0, {\"bin\": 13.0}]", jsonValue.toString());
    }
}