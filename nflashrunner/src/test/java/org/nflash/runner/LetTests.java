package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class LetTests {

    @Test
    public void letTest() throws Exception {
        String query = "From Vision.input_source As $is Let $foo := \"bar\" Select $foo";
        assertEquals("\"bar\"", doQuery(query).toString());
    }

    @Test
    public void let2Test() throws Exception {
        String query = "From Vision.input_source As $is Let $foo := 1 + 2 Select $foo";
        assertEquals("3.0", doQuery(query).toString());
    }

    @Test
    public void let3Test() throws Exception {
        String query = "From Vision.input_source As $is Let $foo := 1 + 2 Select $foo + 2";
        assertEquals("5.0", doQuery(query).toString());
    }

    @Test
    public void let4Test() throws Exception {
        String query = "From Vision.input_source As $is Let $foo := 1 + 2 Select {\"foo\": $foo + 2}";
        assertEquals("{\"foo\": 5.0}", doQuery(query).toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}