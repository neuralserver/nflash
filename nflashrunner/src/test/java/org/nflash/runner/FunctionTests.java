package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class FunctionTests {
    
    @Test
    public void functionTest() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] => array:Count()";
        JsonValue selectResult = doQuery(query);
        assertEquals("3.0", selectResult.toString());
    }

    @Test
    public void functionStmtErrorTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo(): \"foo\" Select => foo(@)";
        assertThrows(Exception.class, () -> {doQuery(query);}, "Function argument count does not match."); // function "foo" does not take arguments
    }
    
    @Test
    public void functionStmtTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo(): \"foo\" Select => foo()";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"foo\"", selectResult.toString());
    }
    
    @Test
    public void functionStmt2Test() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): \"foo\" + $a Select \"bar\" => foo(@)";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"foobar\"", selectResult.toString());
    }
    
    @Test
    public void functionStmtMultiParamTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a, $b): $a + $b Select [1,2,3] : => foo(@, @ + 1)";
        JsonValue selectResult = doQuery(query);
        assertEquals("[3.0, 5.0, 7.0]", selectResult.toString());
    }
    
    @Test
    public void functionStmtRecursionTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): When $a > 0 Then => foo($a - 1) Otherwise $a Select => foo(3)";
        JsonValue selectResult = doQuery(query);
        assertEquals("0.0", selectResult.toString());
    }
    
    @Test
    public void functionStmtMultipleFunctionsTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): \"foo\" + $a Function foo(): \"foo\" Select \"bar\" => foo()";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"foo\"", selectResult.toString());
    }
    
    @Test
    public void functionStmtMultipleFunctions2Test() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): \"foo\" + $a Function foo(): \"foo\" Select \"bar\" => foo(@)";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"foobar\"", selectResult.toString());
    }
    
    @Test
    public void functionStmtArrayTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): $a:(@ + 1) Select [1,2,3] => foo(@)";
        JsonValue selectResult = doQuery(query);
        assertEquals("[2.0, 3.0, 4.0]", selectResult.toString());
    }
    
    @Test
    public void functionStmtArrayFilterTest() throws Exception {
        String query = "From Vision.input_source As $is Function foo($a): $a:(@ + 1) Select [1,2,3] => foo(@) [1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("2.0", selectResult.toString());
    }
    
    @Test
    public void functionStmtJoinTest() throws Exception {
        String query = "From Vision.test Function bookAuthor($bookId): $authors [@.id = $bookId] Let $books := [{\"authorId\": 1, \"name\": \"bin\"}, {\"authorId\": 2, \"name\": \"bai\"}] Let $authors := [{\"id\": 1, \"fname\": \"foo\"}, {\"id\": 2, \"fname\": \"bar\"}] Select $books : {\"book\": @.name,\"author\": (=> bookAuthor(@.authorId)[1].fname)}";
        JsonValue selectResult = doQuery(query);
        assertEquals("[{\"book\": \"bin\", \"author\": \"foo\"}, {\"book\": \"bai\", \"author\": \"bar\"}]", selectResult.toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}