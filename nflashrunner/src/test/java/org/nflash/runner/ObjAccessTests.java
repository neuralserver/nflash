package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class ObjAccessTests {

    @Test
    public void objAccessTest() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\"}.foo";
        assertEquals("\"bar\"", doQuery(query).toString());
    }

    @Test
    public void objAccess2Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\"}.baz";
        assertEquals("{}", doQuery(query).toString());
    }
    
    @Test
    public void objAccess3Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": {\"bar\": \"baz\", \"bin\": \"bai\"}}.foo.bar";
        assertEquals("\"baz\"", doQuery(query).toString());
    }

    @Test
    public void objAccess4Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": [\"bar\"]}.foo";
        assertEquals("[\"bar\"]", doQuery(query).toString());
    }

    @Test
    public void objAccess5Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": null}.foo";
        assertEquals("null", doQuery(query).toString());
    }

    @Test
    public void objAccess6Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": true}.foo";
        assertEquals("true", doQuery(query).toString());
    }
    
    @Test
    public void objAccess7Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": false}.foo";
        assertEquals("false", doQuery(query).toString());
    }
    
    @Test
    public void objAccess8Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": 1 + 2}.foo";
        assertEquals("3.0", doQuery(query).toString());
    }
    
    @Test
    public void objAccess9Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": 1 + 2 * 3}.foo";
        assertEquals("7.0", doQuery(query).toString());
    }

    @Test
    public void objAccess10Test() throws Exception {
        String query = "From Vision.input_source As $is Select [ {\"foo\": true}, {\"foo\": false} ].foo";
        assertEquals("[true, false]", doQuery(query).toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}