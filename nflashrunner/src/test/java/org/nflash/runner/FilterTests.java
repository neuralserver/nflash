package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class FilterTests {
    
    @Test
    public void filterTest() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("12.0", selectResult.toString());
    }

    @Test
    public void filter2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [2]";
        JsonValue selectResult = doQuery(query);
        assertEquals("23.0", selectResult.toString());
    }
    
    @Test
    public void filter3Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [1+1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("23.0", selectResult.toString());
    }
    
    @Test
    public void filter4Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [@ > 20]";
        JsonValue selectResult = doQuery(query);
        assertEquals("[23.0, 34.0]", selectResult.toString());
    }
    
    @Test
    public void filter5Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [@ > (30 + 1)]";
        JsonValue selectResult = doQuery(query);
        assertEquals("[34.0]", selectResult.toString());
    }
    
    @Test
    public void filter6Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [@ > 30 + 1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("[34.0]", selectResult.toString());
    }
    
    @Test
    public void filter7Test() throws Exception {
        String query = "From Vision.input_source As $is Select [12,23,34] [@ > 30 + 4]";
        JsonValue selectResult = doQuery(query);
        assertEquals("[]", selectResult.toString());
    }
    
    @Test
    public void filter8Test() throws Exception {
        String query = "From Vision.input_source As $is Select [{\"foo\": \"bar\"}, {\"foo\": \"baz\"}][1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"foo\": \"bar\"}", selectResult.toString());
    }
    
    @Test
    public void filter9Test() throws Exception {
        String query = "From Vision.input_source As $is Select [{\"foo\": \"bar\"}, {\"foo\": \"baz\"}][1].foo";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"bar\"", selectResult.toString());
    }
    
    @Test
    public void filter10Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"} [@.Key = \"foo\"]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"foo\": \"bar\"}", selectResult.toString());
    }
    
    @Test
    public void filter11Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"} [@.Key = \"bin\"]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"bin\": \"bai\"}", selectResult.toString());
    }
    
    @Test
    public void filter12Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"} [@.Key = \"bin\" Or @.Key = \"foo\"]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"foo\": \"bar\", \"bin\": \"bai\"}", selectResult.toString());
    }

    @Test
    public void filter13Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"} [@.Value = \"baba\"]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"baa\": \"baba\"}", selectResult.toString());
    }
    
    @Test
    public void filter14Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"} [@.Key = \"bin\" Or @.Value = \"baba\"]";
        JsonValue selectResult = doQuery(query);
        assertEquals("{\"bin\": \"bai\", \"baa\": \"baba\"}", selectResult.toString());
    }
    
    @Test
    public void filter15Test() throws Exception {
        String query = "From Vision.input_source As $is Select ([{\"foo\": \"bar\"}, {\"foo\": \"baz\"}] : (@.foo))[1]";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"bar\"", selectResult.toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}