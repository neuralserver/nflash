package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class NflashCompilerTests {
    
    @Test
    public void stringTest() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\"";
        JsonValue selectResult = doQuery(query);
        assertEquals("\"foo\"", selectResult.toString());
    }

    @Test
    public void numberTest() throws Exception {
        String query = "From Vision.input_source As $is Select 777";
        assertEquals("777.0", doQuery(query).toString());
    }

    @Test
    public void nullTest() throws Exception {
        String query = "From Vision.input_source As $is Select null";
        assertEquals("null", doQuery(query).toString());
    }

    @Test
    public void trueTest() throws Exception {
        String query = "From Vision.input_source As $is Select true";
        assertEquals("true", doQuery(query).toString());
    }

    @Test
    public void falseTest() throws Exception {
        String query = "From Vision.input_source As $is Select false";
        assertEquals("false", doQuery(query).toString());
    }

    @Test
    public void objEmptyTest() throws Exception {
        String query = "From Vision.input_source As $is Select { }";
        assertEquals("{}", doQuery(query).toString());
    }

    @Test
    public void objTest() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\"}";
        assertEquals("{\"foo\": \"bar\"}", doQuery(query).toString());
    }

    @Test
    public void obj2Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"count\": 1}";
        assertEquals("{\"foo\": \"bar\", \"count\": 1.0}", doQuery(query).toString());
    }

    @Test
    public void obj3Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\": \"bar\", \"series\": [1,2,3]}";
        assertEquals("{\"foo\": \"bar\", \"series\": [1.0, 2.0, 3.0]}", doQuery(query).toString());
    }
    
    @Test
    public void arrayEmptyTest() throws Exception {
        String query = "From Vision.input_source As $is Select [ ]";
        assertEquals("[]", doQuery(query).toString());
    }

    @Test
    public void arrayTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1, \"bin\"]";
        assertEquals("[1.0, \"bin\"]", doQuery(query).toString());
    }

    @Test
    public void functionCountTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1, \"bin\"] => Count()";
        assertEquals("2.0", doQuery(query).toString());
    }

    @Test
    public void functionCount2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, \"bin\", (2*3^2)] => Count()";
        assertEquals("3.0", doQuery(query).toString());
    }
    
    @Test
    public void functionCountErrorTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 => Count()";
        assertThrows(Exception.class, () -> {doQuery(query).toString();});
    }

    @Test
    public void nmCallResultTest() throws Exception {
        String query = "From Vision.input_source As $is Let $objects := [{\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.8555163145065308, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {},\"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.7907724976539612, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.5533595085144043, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}] Select {\"result\": $objects[1].object_class.name}";
        assertEquals("{\"result\": \"person\"}", doQuery(query).toString());
    }
    
    @Test
    public void nmCallResult2Test() throws Exception {
        String query = "From Vision.input_source As $is Let $objects := [{\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.8555163145065308, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {},\"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.7907724976539612, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.5533595085144043, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}] Select {\"result\": $objects[1].object_class.name, \"count\": $objects => Count()}";
        assertEquals("{\"result\": \"person\", \"count\": 3.0}", doQuery(query).toString());
    }

    @Test
    public void nmCallResult3Test() throws Exception {
        String query = "From Vision.input_source As $is Let $objects := [{\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.8555163145065308, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {},\"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.7907724976539612, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.5533595085144043, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}] Select {\"result\": $objects:({\"name\": @.object_class.name, \"bbox\": @.bbox}), \"count\": $objects => Count()}";
        assertEquals("{\"result\": [{\"name\": \"person\", \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}" +
                     ", {\"name\": \"person\", \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}" +
                     ", {\"name\": \"person\", \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}], \"count\": 3.0}", doQuery(query).toString());
    }
    
    @Test
    public void nmCallResult4Test() throws Exception {
        String query = "From Vision.input_source As $is Let $objects := [{\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.8555163145065308, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {},\"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.7907724976539612, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.5533595085144043, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}] Select $objects:(@.bbox)";
        assertEquals("[[0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]" +
                     ", [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]" +
                     ", [0.0, 0.328752726316452, 1.0, 0.9445611238479614]]", doQuery(query).toString());
    }
    
    @Test
    public void nmCallResult5Test() throws Exception {
        String query = "From Vision.input_source As $is Let $objects := [{\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.8555163145065308, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.29634594917297363, 0.33091506361961365, 0.7512811422348022, 0.5225839018821716]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {},\"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.7907724976539612, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.26732319593429565, 0.047687143087387085, 0.9131156802177429, 0.32385513186454773]}, {\"info\": {\"input\": {\"sourceName\": \"WebInput\", \"dataId\": \"ZL0DMTR3RDYA\", \"sourceDataFrameUri\": \"https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg\", \"ackQueue\": \"input.webdatainputsourcedriver.ack\"}, \"transactionId\": \"L9HWMMMFP2V0\", \"params\": {}, \"ts\": \"2018-12-10 13:28:46.977494\", \"nm\": \"nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17\"}, \"score\": 0.5533595085144043, \"object_class\": {\"id\": 1.0, \"name\": \"person\"}, \"bbox\": [0.0, 0.328752726316452, 1.0, 0.9445611238479614]}] Select $objects:({\"s\": @.score, \"n\": @.object_class.name})";
        assertEquals("[{\"s\": 0.8555163145065308, \"n\": \"person\"}, {\"s\": 0.7907724976539612, \"n\": \"person\"}, {\"s\": 0.5533595085144043, \"n\": \"person\"}]", doQuery(query).toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}