package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class CalculationTests {

    @Test
    public void plusTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 + 2";
        assertEquals("3.0", doQuery(query).toString());
    }

    @Test
    public void plus2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1 + 2 + 3";
        assertEquals("6.0", doQuery(query).toString());
    }

    @Test
    public void plus3Test() throws Exception {
        String query = "From Vision.input_source As $is Select 0 + 0";
        assertEquals("0.0", doQuery(query).toString());
    }

    @Test
    public void minusTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 - 1";
        assertEquals("1.0", doQuery(query).toString());
    }

    @Test
    public void minus2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 - 1 - 1";
        assertEquals("0.0", doQuery(query).toString());
    }

    @Test
    public void multiplicateTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 * 3";
        assertEquals("6.0", doQuery(query).toString());
    }

    @Test
    public void multiplicate2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 * 3 * 4";
        assertEquals("24.0", doQuery(query).toString());
    }

    @Test
    public void divisionTest() throws Exception {
        String query = "From Vision.input_source As $is Select 4 / 2";
        assertEquals("2.0", doQuery(query).toString());
    }
    
    @Test
    public void modulusTest() throws Exception {
        String query = "From Vision.input_source As $is Select 5 % 2";
        assertEquals("1.0", doQuery(query).toString());
    }
    
    @Test
    public void powerTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 ^ 3";
        assertEquals("8.0", doQuery(query).toString());
    }
    
    @Test
    public void powerAssocTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 + 2 ^ 3";
        assertEquals("10.0", doQuery(query).toString());
    }
    
    @Test
    public void powerAssoc2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 ^ 3 + 1";
        assertEquals("9.0", doQuery(query).toString());
    }
    
    @Test
    public void multiplicate3Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 * 3 * 4 * 0";
        assertEquals("0.0", doQuery(query).toString());
    }

    @Test
    public void calculationTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 + 2 * 3";
        assertEquals("7.0", doQuery(query).toString());
    }

    @Test
    public void calculation2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1+2*3^2";
        assertEquals("19.0", doQuery(query).toString());
    }

    @Test
    public void calculation3Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1+(2*3)^2";
        assertEquals("37.0", doQuery(query).toString());
    }

    @Test
    public void calculation4Test() throws Exception {
        String query = "From Vision.input_source As $is Select (1+2)*3^2";
        assertEquals("27.0", doQuery(query).toString());
    }
    
    @Test
    public void calculationParenthesesTest() throws Exception {
        String query = "From Vision.input_source As $is Select (1 + 2) * 3";
        assertEquals("9.0", doQuery(query).toString());
    }
    
    @Test
    public void calculationParentheses2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1 + (2 * 3)";
        assertEquals("7.0", doQuery(query).toString());
    }

    @Test
    public void calculationParentheses3Test() throws Exception {
        String query = "From Vision.input_source As $is Select (2 + 3) * (2 + 3)";
        assertEquals("25.0", doQuery(query).toString());
    }

    @Test
    public void calculationParentheses4Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 * (1 + 2 * (1 + 2))";
        assertEquals("14.0", doQuery(query).toString());
    }

    @Test
    public void calculationParentheses5Test() throws Exception {
        String query = "From Vision.input_source As $is Select (1)";
        assertEquals("1.0", doQuery(query).toString());
    }

    @Test
    public void calculationParentheses6Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 * ((1+2) * (1+2))";
        assertEquals("18.0", doQuery(query).toString());
    }

    @Test
    public void calculationObjTest() throws Exception {
        String query = "From Vision.input_source As $is Select {\"result\": 1 + 2 * 3}";
        assertEquals("{\"result\": 7.0}", doQuery(query).toString());
    }

    @Test
    public void calculationArrayTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1]";
        assertEquals("[1.0]", doQuery(query).toString());
    }
    
    @Test
    public void calculationArray2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1+1]";
        assertEquals("[2.0]", doQuery(query).toString());
    }

    @Test
    public void calculationArray3Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1+2*3, 6/3, 6%5, 3^2, (1+2)*2]";
        assertEquals("[7.0, 2.0, 1.0, 9.0, 6.0]", doQuery(query).toString());
    }

    @Test
    public void negateTest() throws Exception {
        String query = "From Vision.input_source As $is Select -1";
        assertEquals("-1.0", doQuery(query).toString());
    }

    @Test
    public void negate2Test() throws Exception {
        String query = "From Vision.input_source As $is Select - (1 + 2)";
        assertEquals("-3.0", doQuery(query).toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}