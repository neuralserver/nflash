package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class LogicalTests {
    
    @Test
    public void equalsNumberTrueTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 = 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void equalsNumberFalseTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 = 2";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }

    @Test
    public void equalsNullTrueTest() throws Exception {
        String query = "From Vision.input_source As $is Select null = null";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void equalsNullFalseTest() throws Exception {
        String query = "From Vision.input_source As $is Select null = \"null\"";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void equalsObjTrueTest() throws Exception {
        String query = "From Vision.input_source As $is Select {} = {}";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void equalsObjTrue2Test() throws Exception {
        String query = "From Vision.input_source As $is Select {\"foo\":      \"bar\"} = {\"foo\":\"bar\"}";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void equalsObjFalseTest() throws Exception {
        String query = "From Vision.input_source As $is Select {} = []";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void equalsArrayTrueTest() throws Exception {
        String query = "From Vision.input_source As $is Select [] = []";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void equalsArrayTrue2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1] = [1.0]";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void equalsArrayFalseTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1,2] = [1.0]";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void equalsStringTest() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"foo\"";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void equalsString2Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"bar\"";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }

    @Test
    public void gtTest() throws Exception {
        String query = "From Vision.input_source As $is Select 2 > 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void gt2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1 > 2";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }

    @Test
    public void gteTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 >= 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void gte2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 >= 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void gte3Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1 >= 2";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void LtTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 < 2";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void lt2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 < 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }

    @Test
    public void lteTest() throws Exception {
        String query = "From Vision.input_source As $is Select 1 <= 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void lte2Test() throws Exception {
        String query = "From Vision.input_source As $is Select 1 <= 2";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void lte3Test() throws Exception {
        String query = "From Vision.input_source As $is Select 2 <= 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void countTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() = 3.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void count2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() > 2.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void count3Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() >= 3.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void count4Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() < 4.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void count5Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() <= 4.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void count6Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() <= 2.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void count7Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1,3,4] => Count() = 4.0";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void andTest() throws Exception {
        String query = "From Vision.input_source As $is Select (\"foo\" = \"foo\") And (1 = 1)";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void and2Test() throws Exception {
        String query = "From Vision.input_source As $is Select (\"foo\" = \"bar\") And (1 = 1)";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void and3Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"foo\" And 1 = 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void and4Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"foo\" And 1 = 0";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }

    @Test
    public void orTest() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"foo\" Or 1 = 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void or2Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"foo\" Or 1 = 0";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }

    @Test
    public void or3Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"bar\" Or 1 = 1";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    @Test
    public void or4Test() throws Exception {
        String query = "From Vision.input_source As $is Select \"foo\" = \"bar\" Or 1 = 0";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void notTest() throws Exception {
        String query = "From Vision.input_source As $is Select Not true";
        JsonValue selectResult = doQuery(query);
        assertEquals("false", selectResult.toString());
    }
    
    @Test
    public void not2Test() throws Exception {
        String query = "From Vision.input_source As $is Select Not false";
        JsonValue selectResult = doQuery(query);
        assertEquals("true", selectResult.toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}