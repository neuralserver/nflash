package org.nflash.runner.node.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;

public class JsonObjToStringTests {

    @Test
    public void EmptyJsonObjTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        assertEquals("{}", jsonObj.toString());
    }
    
    @Test
    public void JsonObjTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        JsonPair pair = new JsonPair(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"bar\"");
        JsonValue value = new JsonValue(stmnt);
        value.setValue(sNode);
        pair.setPair("\"foo\"", value);
        jsonObj.addPair(pair);
        assertEquals("{\"foo\": \"bar\"}", jsonObj.toString());
    }

    @Test
    public void JsonObjTest2() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        
        JsonPair pair = new JsonPair(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"bar\"");
        JsonValue value = new JsonValue(stmnt);
        value.setValue(sNode);
        pair.setPair("\"foo\"", value);
        jsonObj.addPair(pair);
        
        JsonPair pair2 = new JsonPair(stmnt);
        JsonString sNode2 = new JsonString(stmnt);
        sNode2.setValue("\"bai\"");
        JsonValue value2 = new JsonValue(stmnt);
        value2.setValue(sNode2);
        pair2.setPair("\"bin\"", value2);
        jsonObj.addPair(pair2);

        assertEquals("{\"foo\": \"bar\", \"bin\": \"bai\"}", jsonObj.toString());
    }
    
    @Test
    public void JsonObjTest3() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        
        JsonPair pair = new JsonPair(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"bar\"");
        JsonValue value = new JsonValue(stmnt);
        value.setValue(sNode);
        pair.setPair("\"foo\"", value);
        jsonObj.addPair(pair);
        
        JsonPair pair2 = new JsonPair(stmnt);
        JsonString sNode2 = new JsonString(stmnt);
        sNode2.setValue("\"bai\"");
        JsonValue value2 = new JsonValue(stmnt);
        value2.setValue(sNode2);
        pair2.setPair("\"bin\"", value2);
        jsonObj.addPair(pair2);

        JsonPair pair3 = new JsonPair(stmnt);
        JsonString sNode3 = new JsonString(stmnt);
        sNode3.setValue("\"baba\"");
        JsonValue value3 = new JsonValue(stmnt);
        value3.setValue(sNode3);
        pair3.setPair("\"baa\"", value3);
        jsonObj.addPair(pair3);

        assertEquals("{\"foo\": \"bar\", \"bin\": \"bai\", \"baa\": \"baba\"}", jsonObj.toString());
    }

    @Test
    public void JsonObjWithArrayTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        JsonPair pair = new JsonPair(stmnt);

        JsonArray jsonArray = new JsonArray(stmnt);
        JsonValue value = new JsonValue(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"113\"");
        value.setValue(sNode);
        jsonArray.addValue(value);
        
        pair.setPair("\"foo\"", jsonArray);
        jsonObj.addPair(pair);
        assertEquals("{\"foo\": [\"113\"]}", jsonObj.toString());
    }

    @Test
    public void JsonObjWithArrayTest2() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonObj jsonObj = new JsonObj(stmnt);
        JsonPair pair = new JsonPair(stmnt);

        JsonArray jsonArray = new JsonArray(stmnt);
        JsonValue value = new JsonValue(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"113\"");
        value.setValue(sNode);
        jsonArray.addValue(value);
        
        pair.setPair("\"foo\"", jsonArray);
        jsonObj.addPair(pair);
        
        JsonPair pair2 = new JsonPair(stmnt);
        JsonString sNode2 = new JsonString(stmnt);
        sNode2.setValue("\"bai\"");
        JsonValue value2 = new JsonValue(stmnt);
        value2.setValue(sNode2);
        pair2.setPair("\"bin\"", value2);
        jsonObj.addPair(pair2);
        
        assertEquals("{\"foo\": [\"113\"], \"bin\": \"bai\"}", jsonObj.toString());
    }

}