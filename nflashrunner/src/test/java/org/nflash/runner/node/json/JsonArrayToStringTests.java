package org.nflash.runner.node.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;

public class JsonArrayToStringTests {

    @Test
    public void EmptyJsonArrayTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonArray jsonArray = new JsonArray(stmnt);
        assertEquals("[]", jsonArray.toString());
    }
    
    @Test
    public void JsonArrayTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonArray jsonArray = new JsonArray(stmnt);
        JsonValue value = new JsonValue(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"113\"");
        value.setValue(sNode);
        jsonArray.addValue(value);
        assertEquals("[\"113\"]", jsonArray.toString());
    }
    
    @Test
    public void JsonArrayWithObjTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonArray jsonArray = new JsonArray(stmnt);
        
        JsonObj jsonObj = new JsonObj(stmnt);
        JsonPair pair = new JsonPair(stmnt);
        JsonString sNode = new JsonString(stmnt);
        sNode.setValue("\"bar\"");
        JsonValue value = new JsonValue(stmnt);
        value.setValue(sNode);
        pair.setPair("\"foo\"", value);
        jsonObj.addPair(pair);
        
        jsonArray.addValue(jsonObj);
        assertEquals("[{\"foo\": \"bar\"}]", jsonArray.toString());
    }
}