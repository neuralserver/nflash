package org.nflash.runner.node;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.BinaryNode;
import org.nflash.runner.node.UnaryNode;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;

public class BinaryNodeTests {

    @Test
    public void binaryNodeNumberPlusTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(1));
        JsonNumber rhs = new JsonNumber(select);
        rhs.setValue(new Double(2));
        BinaryNodeProcessor plus = new Plus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(plus);

        Node result = bn.evaluate();
        assertEquals(new Double(3), ((JsonNumber) result).getNumberValue(), "1 + 2 must be 3");
    }
    
    @Test
    public void binaryNodeStringPlusTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonString lhs = new JsonString(select);
        lhs.setValue("\"foo\"");
        JsonString rhs = new JsonString(select);
        rhs.setValue("\"bar\"");
        BinaryNodeProcessor plus = new Plus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(plus);

        Node result = bn.evaluate();
        assertEquals("\"foobar\"", ((JsonString) result).getStringValue(), "foo + bar must be foobar");
    }

    @Test
    public void binaryNodeStringPlus2Test() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(777));
        JsonString rhs = new JsonString(select);
        rhs.setValue("\"bar\"");
        BinaryNodeProcessor plus = new Plus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(plus);

        Node result = bn.evaluate();
        assertEquals("\"777.0bar\"", ((JsonString) result).toString(), "777 + bar must be 777.0bar"); // TODO: trim the result if source number had no decimals
    }
    
    @Test
    public void binaryNodeStringPlus3Test() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonString lhs = new JsonString(select);
        lhs.setValue("\"foo\"");
        JsonNumber rhs = new JsonNumber(select);
        rhs.setValue(new Double(11));
        BinaryNodeProcessor plus = new Plus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(plus);

        Node result = bn.evaluate();
        assertEquals("\"foo11.0\"", ((JsonString) result).getStringValue(), "foo + 11 must be foo11.0"); // TODO: trim the result if source number had no decimals
    }
    
    @Test
    public void binaryNodeNumberMinusTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(1));
        JsonNumber rhs = new JsonNumber(select);
        rhs.setValue(new Double(2));
        BinaryNodeProcessor minus = new Minus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(minus);
        
        Node result = bn.evaluate();
        assertEquals(new Double(-1), ((JsonNumber) result).getNumberValue(), "1 - 2 must be -1");
    }

    @Test
    public void binaryNodeNumberMultiplicateTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(2));
        JsonNumber rhs = new JsonNumber(select);
        rhs.setValue(new Double(3));
        BinaryNodeProcessor mult = new Multiplicate();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(mult);

        Node result = bn.evaluate();
        assertEquals(new Double(6), ((JsonNumber) result).getNumberValue(), "2 * 3 must be 6");
    }

    @Test
    public void binaryNodeNumberPlusMinusTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(1));
        JsonNumber rhs = new JsonNumber(select);
        rhs.setValue(new Double(2));
        BinaryNodeProcessor plus = new Plus();
        BinaryNode bn = new BinaryNode(select);
        bn.setLhs(lhs);
        bn.setRhs(rhs);
        bn.setBinaryNodeProcessor(plus);

        JsonNumber rhs2 = new JsonNumber(select);
        rhs2.setValue(new Double(2));
        BinaryNodeProcessor minus = new Minus();
        BinaryNode bn2 = new BinaryNode(select);
        bn2.setLhs(bn);
        bn2.setRhs(rhs2);
        bn2.setBinaryNodeProcessor(minus);
        
        Node result = bn2.evaluate();
        assertEquals(new Double(1), ((JsonNumber) result).getNumberValue(), "1 + 2 - 2 must be 1");
    }

    @Test
    public void binaryNodeCalcTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement select = new DefaultStatement(ctx);
        JsonNumber lhs = new JsonNumber(select);
        lhs.setValue(new Double(1));
        
        JsonNumber rhs_lhs = new JsonNumber(select);
        rhs_lhs.setValue(new Double(2));
        JsonNumber rhs_rhs = new JsonNumber(select);
        rhs_rhs.setValue(new Double(3));
        
        BinaryNodeProcessor plus = new Plus();
        BinaryNodeProcessor mult = new Multiplicate();

        BinaryNode bnRhs = new BinaryNode(select);
        bnRhs.setLhs(rhs_lhs);
        bnRhs.setRhs(rhs_rhs);
        bnRhs.setBinaryNodeProcessor(mult);

        UnaryNode rhs = new UnaryNode(select);
        rhs.setNode(bnRhs);
        
        BinaryNode root = new BinaryNode(select);
        root.setLhs(lhs);
        root.setRhs(rhs);
        root.setBinaryNodeProcessor(plus);

        Node result = root.evaluate();
        assertEquals(new Double(7), ((JsonNumber) result).getNumberValue(), "1 + (2 * 3) must be 7");
    }
}