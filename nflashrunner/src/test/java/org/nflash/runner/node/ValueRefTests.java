package org.nflash.runner.node.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.Map;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonString;
import org.nflash.runner.node.ValueRef;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.util.JsonUtil;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;

public class ValueRefTests {

    @Test
    public void ValueRefCurrentValueStringTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        Map<String, JsonValue> rtValues = ctx.getRuntimeValues();
        
        JsonValue value = new JsonValue(stmnt);
        JsonString snode = new JsonString(stmnt);
        snode.setValue("\"777\"");
        value.setValue(snode);
        
        rtValues.put("@", value);
        
        ValueRef valueRef = new ValueRef(stmnt);
        valueRef.setValueRef("@"); // TODO: check from grammar that this is ValueRef -value!!!
        
        Node result = valueRef.evaluate();
        
        assertEquals("\"777\"", result.toString());
    }

    @Test
    public void ValueRefCurrentValueObjTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        Map<String, JsonValue> rtValues = ctx.getRuntimeValues();
        
        JsonValue value = JsonUtil.jsonValueFromString("{\"foo\": \"bar\"}");
        
        rtValues.put("@", value);
        
        ValueRef valueRef = new ValueRef(stmnt);
        valueRef.setValueRef("@"); // TODO: check from grammar that this is ValueRef -value!!!
        
        Node result = valueRef.evaluate();
        
        assertEquals("{\"foo\": \"bar\"}", result.toString());
    }

}