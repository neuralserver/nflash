package org.nflash.runner.node.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;

public class JsonNumberTests {
    
    @Test
    public void JsonNumberTest() throws Exception {
        NflashContext ctx = new NflashContext();
        Statement stmnt = new DefaultStatement(ctx);
        JsonNumber nnode = new JsonNumber(stmnt);
        nnode.setValue(new Double(777));
        assertEquals("777.0", nnode.toString());
    }

}