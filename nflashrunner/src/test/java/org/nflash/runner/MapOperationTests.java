package org.nflash.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

import org.nflash.runner.node.Node;
import org.nflash.runner.node.json.JsonNumber;
import org.nflash.runner.node.json.JsonObj;
import org.nflash.runner.node.json.JsonPair;
import org.nflash.runner.node.json.JsonArray;
import org.nflash.runner.node.json.JsonValue;
import org.nflash.runner.processor.BinaryNodeProcessor;
import org.nflash.runner.processor.binary.Plus;
import org.nflash.runner.processor.binary.Minus;
import org.nflash.runner.processor.binary.Multiplicate;
import org.nflash.runner.statement.Statement;
import org.nflash.runner.statement.DefaultStatement;
import org.nflash.runner.NflashContext;
import org.nflash.runner.NflashRunner;

public class MapOperationTests {
    
    @Test
    public void mapOpTest() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2, 3] : @";
        assertEquals("[1.0, 2.0, 3.0]", doQuery(query).toString());
    }

    @Test
    public void mapOp2Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2, 3] : 1";
        assertEquals("[1.0, 1.0, 1.0]", doQuery(query).toString());
    }

    @Test
    public void mapOp3Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2, 3] : \"foo\"";
        assertEquals("[\"foo\", \"foo\", \"foo\"]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp4Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2, 3] : (1 + 1)";
        assertEquals("[2.0, 2.0, 2.0]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp5Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2, 3] : (@ + 1)";
        assertEquals("[2.0, 3.0, 4.0]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp6Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2] : ([@ + 1])";
        assertEquals("[[2.0], [3.0]]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp7Test() throws Exception {
        String query = "From Vision.input_source As $is Select [1, 2] : ( {\"id\": @ + 1, \"orig_id\": @})";
        assertEquals("[{\"id\": 2.0, \"orig_id\": 1.0}, {\"id\": 3.0, \"orig_id\": 2.0}]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp8Test() throws Exception {
        String query = "From Vision.input_source As $is Select ([1, 2] : (@ + 1))";
        assertEquals("[2.0, 3.0]", doQuery(query).toString());
    }
    
    @Test
    public void mapOp9Test() throws Exception {
        String query = "From Vision.input_source As $is Select ([{\"foo\": \"bar\"}, {\"foo\": \"baz\"}] : @.foo)";
        JsonValue selectResult = doQuery(query);
        assertEquals("[\"bar\", \"baz\"]", selectResult.toString());
    }
    
    @Test
    public void mapOp10Test() throws Exception {
        String query = "From Vision.input_source As $is Select ([{\"foo\": \"bar\"}, {\"foo\": \"baz\"}] : (@.foo))";
        JsonValue selectResult = doQuery(query);
        assertEquals("[\"bar\", \"baz\"]", selectResult.toString());
    }
    
    private JsonValue doQuery(String query) throws Exception {
        NflashContext ctx = new NflashContext();
        NflashRunner runner = new NflashRunner();
        runner.setQuery(query);
        ctx = runner.compile(ctx);
        JsonValue selectResult = (JsonValue) ctx.getSelectStatement().evaluate();
        return selectResult;
    }
}