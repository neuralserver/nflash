package org.nflash.dataservice.amqp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.BytesMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import java.nio.charset.StandardCharsets;
import java.io.File;

public class AMQPSender {
    private static final int DEFAULT_COUNT = 10;
    private static final int DELIVERY_MODE = DeliveryMode.NON_PERSISTENT;
    private static final String USER = System.getenv("ARTEMIS_USERNAME");
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD");

    public AMQPSender() { }

    public void sendToQueue(String queueName, byte[] bytesToSend, String messageType, String params, String correlationId) throws Exception {
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        
        //
        // This automatic indirection (by convention, reply-queue is not read from the params) 
        // is used by the neural model calls.
        //
        Destination replyQueue = session.createQueue(queueName + "_reply");

        MessageProducer messageProducer = session.createProducer(queue);

        if (messageType.equals("TEXT")) {
            String replyString = new String(bytesToSend, StandardCharsets.UTF_8);
            TextMessage message = session.createTextMessage( replyString );
            message.setJMSCorrelationID(correlationId);
            messageProducer.send(message);
            System.out.println("TEXT-Message sent: " + replyString);
        }
        
        else if (messageType.equals("BYTES")) {
            BytesMessage message = session.createBytesMessage();
            message.clearBody();
            message.writeBytes(bytesToSend);
            if (params != null) {
                message.setStringProperty("params", params);
            }
            message.setJMSReplyTo(replyQueue);
            message.setJMSCorrelationID(correlationId);
            message.reset();
            messageProducer.send(message);
            System.out.println("BYTES-Message sent.");
        }
        
        connection.close();
    }

    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("AMQPSender :: Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}