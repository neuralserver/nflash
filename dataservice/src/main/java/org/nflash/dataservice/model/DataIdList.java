package org.nflash.dataservice.model;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

public class DataIdList {
    private List<String> dataIds;
    
    public DataIdList() {}
    
    public static DataIdList fromJson(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, DataIdList.class);
    }
    
    public void setDataIds(List<String> dataIds) {
        this.dataIds = dataIds;
    }
    
    public List<String> getDataIds() {
        return this.dataIds;
    }
    
    public String toJsonString() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }
}