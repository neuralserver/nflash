package org.nflash.dataservice.model;

import java.util.Date;

public class DataContainer {
  private String transactionId;
  private Date createdTs;
  private byte[] data;
  
  public DataContainer() {
      this.createdTs = new Date();
  }
  
  public void setTransactionId(String txId) {this.transactionId = txId;}
  public String getTransactionId() {return this.transactionId;}
  public void setData(byte[] data) {this.data = data;}
  public byte[] getData() {return this.data;}
  public Date getCreatedTs() {return this.createdTs;}
}