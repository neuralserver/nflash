package org.nflash.dataservice;

import java.io.IOException;
import java.io.Serializable;
import java.io.File;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.BytesMessage;

import javax.jms.Queue;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import org.nflash.dataservice.util.RandomUtil;
import org.nflash.dataservice.util.JsonUtil;
import org.nflash.dataservice.amqp.AMQPReceiver;
import org.nflash.dataservice.amqp.AMQPSender;
import org.nflash.dataservice.client.model.RegisterDataFrameReply;
import org.nflash.dataservice.model.DataContainer;
import org.nflash.dataservice.model.DataIdList;

import org.nflash.runner.model.NeuralModelCallRequest;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


/**
 * 
 * Save the files received from the queue.
 * 
 */
public class DataService {

    private AMQPReceiver msgLoop;
    private volatile boolean shutdown = false;

    private String contextId;
    private Map<String, DataContainer> idDataContainerMap;
    
    public static void main(String [] args) {
        DataService cs = new DataService();
        cs.start();
    }
    
    DataService() {
        idDataContainerMap = new HashMap<String, DataContainer>();
    }

    public void start() {
        this.shutdown = false;
        System.out.println("DataService started.");
        msgLoop = new AMQPReceiver(this);
        System.out.println("DataService stopped.");
        try {
            msgLoop.start("context"); // Listen to queue named as "context"
        } catch (JMSException jmsException) {
            System.out.println("JMS Error :: " + jmsException.toString());
        } catch (IOException ioe) {
            System.out.println("IOException :: " + ioe.toString());
        } catch (Exception e) {
            System.out.println("Exception :: " + e.toString());
        }
    }
    
    public void shutdown() {
        msgLoop.shutdown();
        this.shutdown = true;
    }
    
    public void handleMessage(Message message) throws Exception, JMSException, IOException {
        if (message == null) {
            System.out.println("Message was null.");
            return;
        }

        if (message.getJMSType() == null) {
            throw new JMSException("JMSType was not set");
        }

        String messageType = message.getJMSType();
        System.out.println("Received message of type :: " + messageType);
        
        System.out.println("Handling message :: " + message);
        
        //
        // Save: read in the uninterpreted bytes and put in to map
        //
        if (messageType.equals("BYTES")) {
            handleBytesMessage(message);
        }
        
        //
        // Return list of context-id:s
        // 
        else if (messageType.equals("LIST")) {
            handleListMessage(message);
        }
        
        //
        // Flush registered dataId:s by transactionId
        //
        else if (messageType.equals("FLUSH")) {
            handleFlushMessage(message);
        }
        
        //
        // Fetch file by given ID and BBox, and return (optionally) clipped file as response.
        //
        else if (messageType.equals("TEXT")) {
            handleTextMessage(message);
        }
        
        // Load test-image, associate it with ID, save into bytebuf, and return id to "context.test" -queue.
        else if (messageType.equals("TEST")) {
            handleTestMessage(message);
        }
    }

    private void handleBytesMessage(Message message) throws Exception, JMSException, IOException {
        String replyQueueName = getJMSReplyTo(message);
        String correlationId = getJMSCorrelationID(message);
        AMQPSender sender = new AMQPSender();
        
        // get message contents (as file), save into server, return ID.
        System.out.println("Getting bytes.");
        BytesMessage bytesMessage = ((BytesMessage) message);
        byte[] bytes = new byte[(int) bytesMessage.getBodyLength()];
        System.out.println("Message length: " + bytesMessage.getBodyLength());
        int numberOfBytesRead = bytesMessage.readBytes(bytes);
        System.out.println("Bytes read.");

        String dataId = RandomUtil.randomAlphaNumeric(12);

        DataContainer dc = new DataContainer();
        dc.setData(bytes);
        getidDataContainerMap().put(dataId, dc);

        System.out.println("Sending result back. DataId: " + dataId);

        String stringResponse = "{\"status\": \"ok\", \"message\": \"\", \"dataId\": \"" + dataId + "\"}";
        RegisterDataFrameReply rdfr = RegisterDataFrameReply.fromJson(stringResponse);
        sender.sendToQueue(replyQueueName, rdfr.toJsonString().getBytes(), "TEXT", null, correlationId); // TODO: make this nicer.
        System.out.println("Done.");
    }
    
    private void handleListMessage(Message message) throws Exception, JMSException, IOException {
        String replyQueueName = getJMSReplyTo(message);
        String correlationId = getJMSCorrelationID(message);
        AMQPSender sender = new AMQPSender();
        
        List<String> dataIdList = new ArrayList(getidDataContainerMap().keySet());
        DataIdList response = new DataIdList();
        response.setDataIds(dataIdList);
        sender.sendToQueue(replyQueueName, response.toJsonString().getBytes(), "TEXT", null, correlationId); // TODO: make this nicer.
        System.out.println("Done.");
    }
    
    private void handleFlushMessage(Message message) throws Exception, JMSException, IOException {
        System.out.println("[FLUSH] Begin.");
        // get transactionId
        String messageBody = ((TextMessage) message).getText();
        String dataIdAndTransactionId = messageBody;
        String[] dataIdAndTransactionIdArr = dataIdAndTransactionId.split(":");
        
        if (dataIdAndTransactionIdArr[0] != null) {
          String dataId = dataIdAndTransactionIdArr[0];
          System.out.println("[FLUSH] Removing by dataId: " + dataId);
          getidDataContainerMap().remove(dataId);
        }
        
        if (dataIdAndTransactionIdArr[1] != null) {
            String transactionId = dataIdAndTransactionIdArr[1];
            System.out.println("[FLUSH] Removing dataId by transactionId: " + transactionId);
            
            // Iterate through dataIdContainerMap, and seek the given transactionId's:
            for (Map.Entry<String, DataContainer> entry : getidDataContainerMap().entrySet()) {
                String dataIdKey = entry.getKey();
                DataContainer dc = entry.getValue();
                System.out.println("Getting key value");

                if (dc.getTransactionId() != null 
                      && dc.getTransactionId().equals(transactionId)) {
                    System.out.println("Trying to remove: " + dataIdKey);
                    getidDataContainerMap().remove(dataIdKey);
                    System.out.println("Removed dataId: " + dataIdKey + ", associated with transactionId: " + transactionId);
                }
            }
        }
        System.out.println("[FLUSH] Done.");
    }
    
    private void handleTextMessage(Message message) throws Exception, JMSException, IOException {
        String replyQueueName = getJMSReplyTo(message);
        String correlationId = getJMSCorrelationID(message);
        AMQPSender sender = new AMQPSender();
        
        String messageBody = ((TextMessage) message).getText();
        System.out.println("DS :: RECEIVED TEXT :: " + messageBody);

        NeuralModelCallRequest nmCallRequest = NeuralModelCallRequest.setFromJson(messageBody);
        System.out.println("[ DATA SERVICE ]=======================");
        System.out.println("Json: " + nmCallRequest.toJsonString());
        System.out.println("=======================================");
        
        String transactionId = nmCallRequest.getTransactionId();
        String dataId = nmCallRequest.getInput().get("dataId"); // requested "dataId", which refers to the bytebuffer.
        String sourceName = nmCallRequest.getInput().getOrDefault("sourceName", ""); // e.g. "folder-input-source-driver"
        String bbox = nmCallRequest.getInput().getOrDefault("bbox", "0,0,0,0"); // "x,y,w,h"
        
        System.out.println("DataService :: bytebuf ID :: " + dataId);
        
        String [] bboxTokens = bbox.split(",");
        
        // TODO: translate bbox to x,y,w,h
        int x = Integer.parseInt(bboxTokens[0].trim());
        int y = Integer.parseInt(bboxTokens[1].trim());
        int w = Integer.parseInt(bboxTokens[2].trim());
        int h = Integer.parseInt(bboxTokens[3].trim());

        DataContainer dc = getidDataContainerMap().get(dataId);
        byte[] bytes = dc.getData();
        
        if (bytes == null) {
            System.out.println("[ DATA SERVICE :: ERROR]===============");
            System.out.println("Bytebuffer not found: " + dataId);
            System.out.println("=======================================");
            String stringResponse = "{\"status\": \"error\", \"message\": \"bytebuf not found\", \"dataId\": \"\"}";
            RegisterDataFrameReply rdfr = RegisterDataFrameReply.fromJson(stringResponse); // FIXME: do not use RegisterDataFrameReply
            sender.sendToQueue(replyQueueName, rdfr.toJsonString().getBytes(), "TEXT", null, correlationId);
        }
        
        else {
            //
            // Clip only if all bbox params are not set to zero.
            //
            byte[] clippedBytes = null; // TODO: there might be problem that clip modifies the original bytes?
            
            if ( ! (x <= 0 && y <= 0 && w <= 0 && h <= 0) ) {
                clippedBytes = clipBytesImage(bytes, x, y, w, h, "jpg"); // TODO: own object for this.
                
                // 
                // Set the clipped bytes into own dataContainer, use the same transactionId as previous.
                //
                DataContainer dcClipped = new DataContainer();
                dcClipped.setTransactionId(transactionId);
                dcClipped.setData(clippedBytes);
                
                String clippedDataId = RandomUtil.randomAlphaNumeric(12);
                idDataContainerMap.put(clippedDataId, dcClipped);
                
                nmCallRequest.getInput().put("id", clippedDataId);
                nmCallRequest.getInput().put("parentId", dataId);
            }

            System.out.println("Sending result to neural model via indirection.");
            // Forwards the Json-parameters as such.
            sender.sendToQueue(replyQueueName, bytes, "BYTES", nmCallRequest.toJsonString(), correlationId);
            System.out.println("Done.");
        }
    }
    
    private void handleTestMessage(Message message) throws Exception, JMSException, IOException {
        String replyQueueName = getJMSReplyTo(message);
        String correlationId = getJMSCorrelationID(message);
        AMQPSender sender = new AMQPSender();
        
        System.out.println("Loading test image.");
        byte[] bytes = loadTestImage();
        System.out.println("Test image loaded.");
        String dataId = RandomUtil.randomAlphaNumeric(12);
        DataContainer dc = new DataContainer();
        dc.setData(bytes);
        getidDataContainerMap().put(dataId, dc);
        String stringResponse = "{\"status\": \"ok\", \"message\": \"\", \"dataId\": \"" + dataId + "\"}";
        RegisterDataFrameReply rdfr = RegisterDataFrameReply.fromJson(stringResponse);
        sender.sendToQueue(replyQueueName, rdfr.toJsonString().getBytes(), "TEXT", null, correlationId);
    }
    
    private String getJMSReplyTo(Message message) throws JMSException {
        if (((Queue) message.getJMSReplyTo()) == null || ((Queue) message.getJMSReplyTo()).getQueueName() == null) {
            throw new JMSException("JMSReplyTo was not set");
        }
        String replyQueueName = ((Queue) message.getJMSReplyTo()).getQueueName();
        return replyQueueName;
    }
    
    private String getJMSCorrelationID(Message message) throws JMSException {
        if (message.getJMSCorrelationID() == null) {
            throw new JMSException("JMSCorrelationID was not set");
        }
        String correlationId = message.getJMSCorrelationID();
        return correlationId;
    }
    
    private byte[] loadTestImage() throws IOException {
        BufferedImage image = ImageIO.read(getClass().getResource("/kirurgi_0.jpg")); // TODO: read from resources -folder.
        // Convert back to bytes.
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String imageType = "jpg";
        ImageIO.write( image, imageType, baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }
    
    // imageType == "jpg" / "png"
    private byte[] clipBytesImage(byte[] bytes, int x, int y, int w, int h, String imageType) throws IOException {
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
        
        // Do some translation, e.g. clip image by bbox.
        BufferedImage subimage = img.getSubimage(x, y, w, h);
        
        // Convert back to bytes.
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( subimage, imageType, baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }

    // TODO: remove
    public void setContextId(String id) {
        this.contextId = id;
    }
    
    public String getContextId() {
        return this.contextId;
    }
    
    public Map<String, DataContainer> getidDataContainerMap() {
        return this.idDataContainerMap;
    }
}