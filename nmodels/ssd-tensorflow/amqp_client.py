from __future__ import print_function, unicode_literals
from proton import Message
from proton.handlers import MessagingHandler
from proton.reactor import Container

#
# https://qpid.apache.org/releases/qpid-proton-0.25.0/proton/python/book/tutorial.html
#
class HelloWorld(MessagingHandler):
    def __init__(self, server, address):
        super(HelloWorld, self).__init__()
        self.server = server
        self.address = address

    def on_start(self, event):
        conn = event.container.connect(self.server)
        event.container.create_receiver(conn, self.address)
        event.container.create_sender(conn, self.address)

    #def on_sendable(self, event):
    #    event.sender.send(Message(body="Hello World!"))
    #    event.sender.close()

    def on_message(self, event):
        print(event.message.body)
        event.connection.close()

Container(HelloWorld("host.docker.internal:5672", "examples")).run()
