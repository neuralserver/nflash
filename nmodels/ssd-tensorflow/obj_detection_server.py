from distutils.version import StrictVersion
import numpy as np
import os
import six.moves.urllib as urllib
import hashlib
import sys
import tarfile
import tensorflow as tf
import zipfile

import json
import datetime
import io

from collections import defaultdict
#from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from utils import label_map_util
from utils import visualization_utils as vis_util

# This is needed to display the images.
%matplotlib inline
#
# AMQP-imports
#
from __future__ import print_function
import optparse
from proton import Message, Url
from proton.handlers import MessagingHandler
from proton.reactor import Container

class ObjDetectionServer(MessagingHandler):
    PATH_TO_TEST_IMAGES_DIR = 'test_images'
    # Size, in inches, of the output images.
    IMAGE_SIZE = (12, 8)
    MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
    # Path to frozen detection graph. This is the actual model that is used for the object detection.
    PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'
    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
    NUM_CLASSES = 90
    
    def __init__(self):
        print("Initializing Detector")
        super(ObjDetectionServer, self).__init__()
        
        artemis_user = os.environ['ARTEMIS_USERNAME']
        artemis_password = os.environ['ARTEMIS_PASSWORD']
        
        self.url = artemis_user + ":" + artemis_password + "@artemis:5672" # "host.docker.internal:5672"
        self.address = "nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17"
        self.reply_address = "nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17_reply"
        
        # This is needed since the notebook is stored in the object_detection folder.
        sys.path.append("..")
        
        if (not os.path.isfile(self.PATH_TO_FROZEN_GRAPH)):
            self.download_model()

        if StrictVersion(tf.__version__) < StrictVersion('1.9.0'):
          raise ImportError('Please upgrade your TensorFlow installation to v1.9.* or later!')

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
          od_graph_def = tf.GraphDef()
          with tf.gfile.GFile(self.PATH_TO_FROZEN_GRAPH, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=self.NUM_CLASSES, use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)
    
    def download_model(self):
        # What model to download.
        MODEL_FILE = self.MODEL_NAME + '.tar.gz'
        DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
        print("Downloading model")
        opener = urllib.request.URLopener()
        opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
        tar_file = tarfile.open(MODEL_FILE)

        for file in tar_file.getmembers():
          file_name = os.path.basename(file.name)
          if 'frozen_inference_graph.pb' in file_name:
            tar_file.extract(file, os.getcwd())
        
    def load_image_into_numpy_array(self, image):
      (im_width, im_height) = image.size
      if image.format == 'PNG':
          image = image.convert('RGB')
      return np.array(image.getdata()).reshape(
          (im_height, im_width, 3)).astype(np.uint8)

    # https://qpid.apache.org/releases/qpid-proton-0.25.0/proton/python/book/tutorial.html
    def on_start(self, event):
        print("Listening on", self.url)
        self.container = event.container
        self.conn = event.container.connect(self.url)
        self.receiver = event.container.create_receiver(self.conn, self.address)
        self.server = self.container.create_sender(self.conn, None)
   
    def on_message(self, event):
        print("Received", event.message)
        if event.message.content_type == "application/octet-stream":          
            nm_params = json.loads(event.message.properties["params"])
            print("PARAMS :: " + str(nm_params))
            
            image = Image.open(io.BytesIO(event.message.body))
            # the array based representation of the image will be used later in order to prepare the
            # result image with boxes and labels on it.
            image_np = self.load_image_into_numpy_array(image)
            
            output_dict = self.detect(image_np) # run detection-algorithm
            # self.visualize_detection(image_np, output_dict)
            plt.imshow(image_np)
            
            result_json = self.detection_to_json(nm_params, output_dict)
            
            reply_addr = self.reply_address
            
            if event.message.reply_to is not None:
              reply_addr = event.message.reply_to
            
            print("Sending detection to MQ :: " + reply_addr)
            print(result_json)
            #exit()
            message_time_to_live_in_seconds = 10
            self.server.send(Message(address=reply_addr, body=result_json,
                                correlation_id=event.message.correlation_id, ttl=message_time_to_live_in_seconds))
        else:
            print("Unsupported content_type, please use application/octet-stream.")
        
    def prepare_detection(self, image_path):
      print ("PROCESSING IMAGE :: " + image_path)
      image = Image.open(image_path)
      # the array based representation of the image will be used later in order to prepare the
      # result image with boxes and labels on it.
      image_np = self.load_image_into_numpy_array(image)
      return image_np
    
    def detect(self, image_np):
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      # Actual detection.
      output_dict = self.run_inference_for_single_image(image_np, self.detection_graph)
      return output_dict
    
    def detection_to_json(self, amqp_message, output_dict):
      amqp_message["ts"] = str(datetime.datetime.utcnow())
      amqp_message["nm"] = self.address
      
      param_include_list = None
      param_exclude_list = None
      wanted_accuracy = 0.50
      
      nm_params = amqp_message.get("params", None)
      
      if nm_params is not None:
        if "include" in nm_params:
          param_include_list = nm_params["include"]
      
        if "exclude" in nm_params:
          param_exclude_list = nm_params["exclude"]
    
        if "accuracy" in nm_params:
          wanted_accuracy = nm_params["accuracy"]
    
      results = []
      
      for i in range (0, len(output_dict['detection_boxes'])):
        if (output_dict['detection_scores'][i] > wanted_accuracy):
          object_id = output_dict['detection_classes'][i]
          object_class = self.category_index[object_id] # jsonObj: {id, name}
          include_object = False
        
          if (param_include_list is None and param_exclude_list is None):
            include_object = True
          
          elif (param_include_list is not None and object_class["name"] in param_include_list):
            include_object = True
          
          elif (param_exclude_list is not None and object_class["name"] not in param_exclude_list):
            include_object = True
            
          if (include_object):
            results.append(json.dumps({
              "info": amqp_message,
              "object_class": object_class,
              "bbox": output_dict['detection_boxes'][i].tolist(),
              "score": float(output_dict['detection_scores'][i])
            }))
    
      return results
      
    # @param image_np : numpy array
    #
    # NOTE: run visualization only when debugging. Otherwise they'll remain in
    #       the memory unless explicitly close.
    #
    def visualize_detection(self, image_np, output_dict):
        print("Visualizing detection.")
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            self.category_index,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=8)
        plt.figure(figsize=self.IMAGE_SIZE)
        plt.imshow(image_np)

    def run_inference_for_single_image(self, image, graph):
      with graph.as_default():
        with tf.Session() as sess:
          # Get handles to input and output tensors
          ops = tf.get_default_graph().get_operations()
          all_tensor_names = {output.name for op in ops for output in op.outputs}
          tensor_dict = {}
          for key in [
              'num_detections', 'detection_boxes', 'detection_scores',
              'detection_classes', 'detection_masks'
          ]:
            tensor_name = key + ':0'
            if tensor_name in all_tensor_names:
              tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                  tensor_name)
          if 'detection_masks' in tensor_dict:
            # The following processing is only for single image
            detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
            detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
            # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
            real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
            detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
            detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
            detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
                detection_masks, detection_boxes, image.shape[0], image.shape[1])
            detection_masks_reframed = tf.cast(
                tf.greater(detection_masks_reframed, 0.5), tf.uint8)
            # Follow the convention by adding back the batch dimension
            tensor_dict['detection_masks'] = tf.expand_dims(
                detection_masks_reframed, 0)
          image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

          # Run inference
          output_dict = sess.run(tensor_dict,
                                 feed_dict={image_tensor: np.expand_dims(image, 0)})

          # all outputs are float32 numpy arrays, so convert types as appropriate
          output_dict['num_detections'] = int(output_dict['num_detections'][0])
          output_dict['detection_classes'] = output_dict[
              'detection_classes'][0].astype(np.uint8)
          output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
          output_dict['detection_scores'] = output_dict['detection_scores'][0]
          if 'detection_masks' in output_dict:
            output_dict['detection_masks'] = output_dict['detection_masks'][0]
      return output_dict

try:
    Container(ObjDetectionServer()).run()
except KeyboardInterrupt: pass
