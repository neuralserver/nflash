#
# This prints the output as Json, instead of rendering it in the Jupyter-notebook.
#
# TODO: Read request from message-queue (AMQP), check what is the input file that
#       should be loaded, do detection (infer), and write result back to the queue.
#
def detect():
  WANTED_ACCURACY = 0.50 # sloppy
  for image_path in TEST_IMAGE_PATHS:
    print ("PROCESSING IMAGE :: " + image_path)
    image = Image.open(image_path)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    output_dict = run_inference_for_single_image(image_np, detection_graph)
    results = []
    for i in range (0, len(output_dict['detection_boxes'])):
      if (output_dict['detection_scores'][i] > WANTED_ACCURACY):
        object_id = output_dict['detection_classes'][i]
        results.append(json.dumps({
          "object_class": category_index[object_id],
          "bbox": output_dict['detection_boxes'][i].tolist(),
          "score": float(output_dict['detection_scores'][i])
        }))
    
    print (results)
