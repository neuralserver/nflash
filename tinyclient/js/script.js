//neuralQuery = "QUERY TEXT From Vision.webcam_a As $wa Select $wa -> nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17 {\"include\": [\"person\", \"clock\"]}";
//neuralQuery = "QUERY TEXT From Vision.webcam_a As $wa Select $wa -> nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17 {\"exclude\": [\"person\", \"clock\"]}";
//neuralQuery = "QUERY TEXT From Vision.webcam_a As $wa Select $wa -> nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17";

ws = new WebSocket("ws://localhost:9080");

ws.onopen = function(event) {
    // no-op
};

// Send some data to the queue that was opened with above query:
var image_data = [
    "https://www.kuntaliitto.fi/sites/default/files/styles/expert_page/public/media/editor_managed/kirurgi_0.jpg",
    "https://blog.keras.io/img/imgclf/cats_and_dogs.png",
    "https://i.cbc.ca/1.4050687.1490994631!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/woman-huge-umbrella.jpg",
    "https://ichef.bbci.co.uk/food/ic/food_16x9_832/recipes/quick_pepperoni_pizza_64616_16x9.jpg",
    "https://d1nz104zbf64va.cloudfront.net/dt/a/o/top-7-books-that-changed-the-world.jpg",
    "http://static3.uk.businessinsider.com/image/5829e82f848fb61174ffc515/you-can-sleep-at-this-5-million-apartment-inside-londons-st-pancras-clock-tower.jpg",
    "https://s-media-cache-ak0.pinimg.com/originals/0e/2e/e3/0e2ee3cd816c66a2b600f7f502b4a903.jpg"
];
var image_index = 0;
var image_max_index = image_data.length;

var sendData = function (data) {
    ws.send(data);
};

ws.onmessage = function(event) {
    console.log("Received data: " + event.data);
    var div = document.getElementById('query-log');
    div.innerHTML += '<p>' + event.data + '<hr/></p>';
    if (event.data.startsWith("OUTPUT RESULT=")) {
        var jsonDataStr = event.data.substring("OUTPUT RESULT=".length);
        var jsonData = JSON.parse(jsonDataStr);
        if (jsonData.length >= 1) {
            var imageUrl = jsonData[0].info.input.sourceDataFrameUri;
            document.getElementById('display-image').src = imageUrl;
        }
        document.getElementById('image-objects').innerHTML = "";
        jsonData.forEach(function (result) {
            var objName = result.object_class.name;
            console.log(objName);
            var pEl = document.createElement("p");
            var textNode = document.createTextNode(objName);
            pEl.appendChild(textNode);
            document.getElementById('image-objects').appendChild(pEl);
        });
    }
};

// First wait until the document has been loaded:
document.addEventListener("DOMContentLoaded", function(event) { 
    // no-op
});

var sendQuery = function () {
    var command = document.getElementById('command');
    var selectedCommand = command.options[command.selectedIndex].text;
    var query = document.getElementById('neural-query').value;
                    
    sendData(selectedCommand + " " + query);
};

var sendInputSourceDataRequest = function () {
    var command = 'INPUT';
    var request = document.getElementById('input-source-driver').value;
    
    sendData(command + " " + request);
};

var displayExample = function () {
    var command = document.getElementById('command');
    var selectedCommand = command.options[command.selectedIndex].text;
    
    var example = document.getElementById('example-query');
    if (selectedCommand === 'INPUT') {
        example.innerHTML = 'webcam_a {"dataId": "obtained from data-service","sourceName": "activemqtest","sourceDataFrameUri": "testbuf"}';
    } else if (selectedCommand === 'STOP') {
        example.innerHTML = 'STOP queryId';
    } else if (selectedCommand === 'START') {
        example.innerHTML = 'START queryId';
    } else if (selectedCommand === 'QUERY TEXT') {
        example.innerHTML = 'From Vision.webcam_a As $wa Select $wa -> nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17 {"include": ["person", "clock"]}';
    } else if (selectedCommand === 'QUERY LIST') {
        example.innerHTML = 'Display query list'
    } else if (selectedCommand === 'SHOW') {
        example.innerHTML = 'Display query'
    } else if (selectedCommand === 'SUBSCRIBE') {
        example.innerHTML = 'Subscribe to query'
    } else if (selectedCommand === 'UNSUBSCRIBE') {
        example.innerHTML = 'Unsubscribe from query'
    } else if (selectedCommand === 'DELETE') {
        example.innerHTML = 'DELETE queryId'
    }
};