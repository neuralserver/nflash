// V. 3
//
// Define a grammar called Nflash
//
grammar Nflash;

nf
    : from
      window_by?
      function_stmt*
      let_stmt*
      select
      group_by?
      order_by?
      stop_stmt?
    ;

from
    : 'From' input_source json_obj? ('As' input_source_alias)?
    ;

window_by
    : 'Window By' expr
    ;

function_stmt
    : 'Function' (ID ':')? ID '(' (CONST_ID? | (CONST_ID ',' CONST_ID)+) ')' ':' let_stmt* expr
    ;

let_stmt
    : 'Let' CONST_ID ':=' expr
    ;

select
    : 'Select' expr
    ;

group_by
    : 'Group By' expr
    ;

order_by
    : 'Order By' expr
    ;

stop_stmt
    : 'Stop' condition
    ;

expr
    : (json (filter_expr | obj_access)*
    | input_source (filter_expr | obj_access)*
    | neural_model_call (filter_expr | obj_access)* // nmCall must be before value_ref
    | value_ref (filter_expr | obj_access)*
    | function_call (filter_expr | obj_access)*
    | condition
    | '(' expr ')' (filter_expr | obj_access)*)+
    | MINUS expr
    | expr MAP expr
    | <assoc=right> expr POW expr
    | expr MULT expr
    | expr DIV expr
    | expr MOD expr
    | expr (PLUS | MINUS) expr
    | expr (EQ | LT | GT | LTE | GTE) expr
    | expr (AND | OR) expr
    | NOT expr
    ;

obj_access
    : OBJ_ACCESSOR ID
    ;

condition
    : ('When' expr 'Then' expr)+ 'Otherwise' expr
    ;

// TODO: add splicing expr
filter_expr
    : '[' expr ']'
    ;

function_call
    : '=>' (ID ':')? ID '(' (expr? | (expr ',' expr)+) ')'
    ;

// Could rename this to "integration_call"
neural_model_call
    : '->' neural_model_name json_obj?
    ;

input_source
    : input_system '.' input_source_name
    ;

input_system
    : ('Vision' | 'Memory')
    ;

input_source_name
    : ID
    ;

// Used when declaring constant, e.g. "Let $foo := 'bar'", here the "$foo"
// is const_binding.
//const_binding
//    : CONST_ID
//    ;

// E.g. "$wa", "$foo", @, or $
value_ref
    : CURRENT_VALUE | DATAFRAME_VALUE | CONST_ID
    ;

input_source_alias
    : CONST_ID
    ;

neural_model_name
    : NEURAL_MODEL_ID
    ;

// ====================JSON=================================
json
   : json_value
   ;

json_obj
   : '{' json_pair (',' json_pair)* '}'
   | '{' '}'
   ;

json_pair
   : STRING ':' (json_value | expr)
   ;

json_array
   : '[' (json_value | expr) (',' (json_value | expr))* ']'
   | '[' ']'
   ;

json_value
   : json_obj
   | json_array
   | (STRING
     | NUMBER
     | JSON_TRUE
     | JSON_FALSE
     | JSON_NULL)
   ;
// =====================================================

CONST_ID
    : [\\$][a-zA-Z]+[a-zA-Z_]*
    ;

OBJ_ACCESSOR
    : '.'
    ;

NEURAL_MODEL_ID
    : 'nm.' [a-zA-Z][0-9a-zA-Z\\._]+
    ;

CURRENT_VALUE
    : '@'
    ;

DATAFRAME_VALUE
    : '$'
    ;

MAP
    : ':'
    ;

AND
    : 'And'
    ;
    
OR
    : 'Or'
    ;

NOT
    : 'Not'
    ;
    
EQ
    : '='
    ;

LT
    : '<'
    ;

GT
    : '>'
    ;

LTE
    : '<='
    ;

GTE
    : '>='
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MULT
    : '*'
    ;

DIV
    : '/'
    ;

POW
    : '^'
    ;

MOD
    : '%'
    ;

WS
    : [ \t\r\n]+ -> skip
    ;

// ============= JSON================

STRING
   : '"' (ESC | SAFECODEPOINT)* '"'
   ;


fragment ESC
   : '\\' (["\\/bfnrt] | UNICODE)
   ;


fragment UNICODE
   : 'u' HEX HEX HEX HEX
   ;


fragment HEX
   : [0-9a-fA-F]
   ;


fragment SAFECODEPOINT
   : ~ ["\\\u0000-\u001F]
   ;


NUMBER
   : '-'? INT ('.' [0-9] +)? EXP?
   ;


fragment INT
   : '0' | [1-9] [0-9]*
   ;

JSON_TRUE
    : 'true'
    ;

JSON_FALSE
    : 'false'
    ;

JSON_NULL
    : 'null'
    ;

ID
    : [a-zA-Z_]+
    ;

// no leading zeros
fragment EXP
   : [Ee] [+\-]? INT
   ;

// \- since - means "range" inside [...]
