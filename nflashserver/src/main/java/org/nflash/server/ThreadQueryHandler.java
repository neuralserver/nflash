package org.nflash.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.HashMap;
import org.nflash.runner.NflashRunner;

public class ThreadQueryHandler implements QueryHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadQueryHandler.class);
    
    private Thread thread;
    private NflashRunner runner;
    
    public ThreadQueryHandler() { }
    
    public String startQuery(NflashRunner runner) {
        stopQuery();
        this.runner = runner;
        String threadId = this.runner.getQueryId();
        thread = new Thread(runner, threadId);
        
        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread th, Throwable ex) {
                System.out.println("Uncaught exception: " + ex);
                System.out.println("Stopping thread :: " + th.getId());
                NflashServer.getQueryMap().get(threadId).setQueryStatus(QueryStatus.ERROR);
                th.stop();
            }
        };
        
        thread.setUncaughtExceptionHandler(h);
        thread.start();
        LOGGER.info("Started query: {}", threadId);
        return threadId;
    }
    
    public void stopQuery() {
        if (runner != null) {
            runner.shutdown();
        }
        if (thread != null) {
            thread.interrupt();
        }
    }

}