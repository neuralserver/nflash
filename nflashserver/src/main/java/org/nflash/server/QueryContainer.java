package org.nflash.server;

import org.nflash.runner.NflashRunner;

// 
// Contains a single registered query.
// 
public interface QueryContainer {

    // Returns handle for the query (ID)
    public String addQuery(String query); // Is added, but not started

    public String getQuery();
    
    public void startQuery();
    
    public void stopQuery();
    
    public QueryStatus getQueryStatus();
    
    public void setQueryStatus(QueryStatus status);
    
}