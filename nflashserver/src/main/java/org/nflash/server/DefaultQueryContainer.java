package org.nflash.server;

import org.nflash.runner.NflashRunner;
import org.nflash.server.util.RandomUtil;

// 
// Contains a single registered query.
// 
public class DefaultQueryContainer implements QueryContainer {

    private NflashRunner runner;
    private QueryHandler handler;
    private QueryStatus status;
    private String queryThreadId;
    
    public DefaultQueryContainer() {
        this.handler = new ThreadQueryHandler();
        this.status = QueryStatus.STOPPED;
    }
    
    // Returns handle for the query (ID)
    public String addQuery(String query) {
        this.runner = new NflashRunner();
        this.runner.setQuery(query);
        queryThreadId = RandomUtil.randomAlphaNumeric(10);
        this.runner.setQueryId(queryThreadId);
        this.status = QueryStatus.READY;
        return queryThreadId;
    }

    public void startQuery() {
        handler.startQuery(this.runner);
        this.status = QueryStatus.STARTED;
    }
    
    public void stopQuery() {
        handler.stopQuery();
        this.status = QueryStatus.STOPPED;
    }
    
    public QueryStatus getQueryStatus() {
        return this.status;
    }
    
    public void setQueryStatus(QueryStatus status) {
        this.status = status;
    }
    
    public String getQuery() {
        return this.runner.getQuery();
    }
}