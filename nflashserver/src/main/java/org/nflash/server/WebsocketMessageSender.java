package org.nflash.server;

import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import io.netty.util.concurrent.Promise;
import io.netty.util.concurrent.PromiseCombiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import org.nflash.runner.NflashRunner;

public class WebsocketMessageSender implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebsocketMessageSender.class);
    
    //
    // All channels.
    //
    private ChannelGroup channels;

    //
    // The output-queue that this starts to listen to.
    // One WebsocketMessageSender is bound to one channel.
    //
    private String outputQueue;
    
    public WebsocketMessageSender(ChannelGroup channels) {
        this.channels = channels;
        LOGGER.info("Starting new thread, which listens output-queue.");
    }
    
    public void run() {
        //
        // Receiver uses "this.sendText" to send the received message to the websocket.
        //
        AMQPOutputReceiver outputReceiver = new AMQPOutputReceiver(this);
        try {
            if (getOutputQueue() != null) {
                outputReceiver.start("nql_output_" + getOutputQueue());
            } else {
                throw new Exception("Cannot start thread: no output queue.");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void sendText(String textMessage, String command, ChannelHandlerContext ctx, String queryId) {
        String textFrame = queryId + " " + command + " RESULT=" + textMessage; // TODO: send as Json => Define Json-model.
        TextWebSocketFrame frame = new TextWebSocketFrame(textFrame);
        PromiseCombiner promiseCombiner = new PromiseCombiner();
        
        this.channels.stream()
                .filter(channel -> {
                    //
                    // If OUTPUT-command, then send the frame only to subscribers (channels obtained from map).
                    //
                    if (command.equals("OUTPUT")) {
                        List<String> ctxIdListFromMap = NflashServer.getQueryContextMap().
                                            get(queryId.substring(11, queryId.length())); // First remove the "nql_output_" -part.
                        if (ctxIdListFromMap != null) {
                            if (ctxIdListFromMap.contains(channel.id().asShortText())) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                    //
                    // Otherwise, send to context-channel.
                    //
                    else {
                        return channel == ctx.channel();
                    }
                })
                .forEach(channel -> {
                    frame.retain();
                    promiseCombiner.add(channel.writeAndFlush(frame.duplicate()).addListener(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture future) throws Exception {
                            if (!future.isSuccess()) {
                                LOGGER.info("Failed to write to channel: {}", future.cause());
                            }
                        }
                    }));
                });
    }

    public void setOutputQueue(String outputQueue) {
        this.outputQueue = outputQueue;
    }
    
    public String getOutputQueue() {
        return this.outputQueue;
    }

}