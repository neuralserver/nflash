package org.nflash.server;

public enum QueryStatus {
    READY,
    STARTED,
    STOPPED,
    ERROR
}