package org.nflash.server;

import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import io.netty.util.concurrent.Promise;
import io.netty.util.concurrent.PromiseCombiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.nflash.server.model.InputSourceDataRequest;
import org.nflash.runner.NflashRunner;
import org.nflash.runner.amqp.AMQPSender;
import org.nflash.runner.model.VisionInputRequest;

// NOTE: this instance may be shared between multiple ChannelHandlerContext's.
public class WebSocketMessageHandler extends SimpleChannelInboundHandler<WebSocketFrame> {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketMessageHandler.class);

    private static final ChannelGroup allChannels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private WebsocketMessageSender sender;

    // <QueryId, [ ChannelHandlerContext->Channel.id ]>
    private Map<String, List<String>> queryContextMap; // shared.

    public WebSocketMessageHandler() {
        super(false);
        
        sender = new WebsocketMessageSender(allChannels);
    }
    
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
        if (frame instanceof TextWebSocketFrame) {
            final String text = ((TextWebSocketFrame) frame).text();
            
            LOGGER.info("Received text frame {}", text);
            
            if (text.startsWith("QUERY FILE")) {
                throw new Exception("Not implemented yet.");
                /*
                String fileName = text.substring(10, text.length());
                File file = new File(fileName);
                // TODO: read file contents.
                String query = "TODO: read from file";
                QueryContainer qc = new DefaultQueryContainer(queryHandler);
                String queryId = qc.addQuery(query);
                getQueryMap().put(queryId, qc);
                LOGGER.info("Query file: {}", fileName);
                sender.sendText("QUERY FILE RESULT={\"queryId\": \"" + queryId + "\", \"status\": \"" + getQueryMap().get(queryId).getQueryStatus() + "\"}");
                */
            }
            
            else if (text.startsWith("QUERY TEXT")) {
                String command = "QUERY TEXT";
                String query = text.substring(10, text.length());
                QueryContainer qc = new DefaultQueryContainer();
                String queryId = qc.addQuery(query);
                getQueryMap().put(queryId, qc);
                LOGGER.info("Query: {}", query);
                sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"" + getQueryMap().get(queryId).getQueryStatus() + "\"}", command, ctx, queryId);
            }
            
            else if (text.startsWith("STOP")) {
                String command = "STOP";
                String queryId = text.substring(4, text.length()).trim();
                LOGGER.info("Stopping query: {}", queryId);
                if (getQueryMap().get(queryId) != null && getQueryMap().get(queryId).getQueryStatus() == QueryStatus.STARTED) {
                    getQueryMap().get(queryId).stopQuery();
                    LOGGER.info("Stopped query: {}", queryId);
                    sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"" + getQueryMap().get(queryId).getQueryStatus() + "\"}", command, ctx, queryId);
                }
            }
            
            else if (text.startsWith("START")) {
                String command = "START";
                String queryId = text.substring(5, text.length()).trim();
                LOGGER.info("Starting query: {}", queryId);
                if (getQueryMap().get(queryId) != null && (getQueryMap().get(queryId).getQueryStatus() == QueryStatus.STOPPED || 
                                                           getQueryMap().get(queryId).getQueryStatus() == QueryStatus.READY)) {
                    
                    sender.setOutputQueue(queryId);
                    Thread websocketSenderThread = new Thread(sender, "Websocket sender thread ");
                    websocketSenderThread.start();
                
                    getQueryMap().get(queryId).startQuery();
                    LOGGER.info("Started query: {}", queryId);
                    sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"" + getQueryMap().get(queryId).getQueryStatus() + "\"}", command, ctx, queryId);
                }
            }
            
            // Show query as plain text.
            else if (text.startsWith("SHOW")) {
                String command = "SHOW";
                String queryId = text.substring(4, text.length()).trim();
                LOGGER.info("Display query: {}", queryId);
                if (getQueryMap().get(queryId) != null) {
                    String query = getQueryMap().get(queryId).getQuery();
                    LOGGER.info("Query: {}", query);
                    sender.sendText("{\"queryId\": \"" + queryId + 
                            "\", \"status\": \"" + getQueryMap().get(queryId).getQueryStatus() + "\","
                            + "\"query\": \"" + query + "\"}", command, ctx, queryId);
                }
            }
            
            // Delete given query. Query must be stopped first.
            else if (text.startsWith("DELETE")) {
                String command = "DELETE";
                String queryId = text.substring(6, text.length()).trim();
                LOGGER.info("Deleting query: {}", queryId);
                if (getQueryMap().get(queryId) != null && getQueryMap().get(queryId).getQueryStatus() == QueryStatus.STOPPED) {
                    getQueryMap().remove(queryId);
                    // TODO: stop the associated sender as well?
                    LOGGER.info("Deleted query: {}", queryId);
                    sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"DELETED\"}", command, ctx, queryId);
                } else if (getQueryMap().get(queryId) != null && getQueryMap().get(queryId).getQueryStatus() == QueryStatus.STARTED) {
                    sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"Query must be stopped first.\"}", command, ctx, queryId);
                } else {
                    sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"Query not found.\"}", command, ctx, queryId);
                }
            }
            
            // SUBSCRIBE: bind query result (output) with the websocket-client
            else if (text.startsWith("SUBSCRIBE")) {
                String queryId = text.substring(9, text.length()).trim();
                String command = "SUBSCRIBE";
                // TODO: save context. TODO: save multiple queryId under same ctx.
                
                List<String> ctxList = new ArrayList<String>();
                
                if (getQueryContextMap().get(queryId) != null) {
                    ctxList = getQueryContextMap().get(queryId);
                }
                
                String ctxId = ctx.channel().id().asShortText();
                
                ctxList.add(ctxId);
                LOGGER.info("======> ADDED CTX :: " + ctxId);
                getQueryContextMap().put(queryId, ctxList); // Give this to sender.
                
                LOGGER.info("Bind to ws-client query: {}, {}", queryId, getQueryContextMap());
                sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"SUBSCRIBED\"}", command, ctx, queryId);
            }

            else if (text.startsWith("UNSUBSCRIBE")) {
                String command = "UNSUBSCRIBE";
                String queryId = text.substring(11, text.length()).trim();
                LOGGER.info("Unbind to ws-client query: {}, {}", queryId, getQueryContextMap());
                
                if (getQueryContextMap().get(queryId) != null) {
                    String ctxId = ctx.channel().id().asShortText();
                    getQueryContextMap().get(queryId).remove(ctxId);
                }
                // TODO: wsSender.interupt();
                sender.sendText("{\"queryId\": \"" + queryId + "\", \"status\": \"UNSUBSCRIBED\"}", command, ctx, queryId);
            }
            
            // 
            // TODO: Should we still accept input if there are no queries running?
            // 
            // InputSourceDataRequest, tells input-source-driver to start pulling data
            // and feeding that to the wanted input-queue.
            else if (text.startsWith("INPUT")) {
                // INPUT JSON-obj
                String inputSourceDataRequestJson = text.substring(5, text.length()).trim();
                InputSourceDataRequest inputSourceDataRequest = InputSourceDataRequest.fromJson(inputSourceDataRequestJson);
                String inputSourceDriver = inputSourceDataRequest.getInputSourceDriver();
                
                // FIXME: use own, internal amqpSender, not one from runner.
                AMQPSender amqpSender = new AMQPSender();
                amqpSender.sendToQueue(inputSourceDriver, inputSourceDataRequest.toJsonString());
            }
            
            else if (text.startsWith("QUERY LIST")) {
                LOGGER.info("Listing the queries :: " + getQueryMap().toString());
                String command = "QUERY LIST";
                for (Map.Entry<String, QueryContainer> entry : getQueryMap().entrySet()) {
                    String queryId = entry.getKey();
                    LOGGER.info("Query ID :: " + queryId + ". Status :: " + entry.getValue().getQueryStatus().toString());
                    // TODO: send as a single Json-array:
                    sender.sendText("{\"queryId\": \"" + entry.getKey() +  "\", \"status\": \"" + entry.getValue().getQueryStatus().toString() + "\"}", command, ctx, queryId);
                }
            }
            
            // TODO: command for "QUERY INFO" (show query statistics and subscribers)
            
            else {
                LOGGER.info("Unknown command: {}", text);
            }

        } else {
            throw new UnsupportedOperationException("Invalid websocket frame received");
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info("Adding new channel {} to list of channels", ctx.channel().remoteAddress());
        allChannels.add(ctx.channel());
    }

    public Map<String, QueryContainer> getQueryMap() {
        return NflashServer.getQueryMap();
    }
    
    public Map<String, List<String>> getQueryContextMap() {
        return NflashServer.getQueryContextMap();
    }
}