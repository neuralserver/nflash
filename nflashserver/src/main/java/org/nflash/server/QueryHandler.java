package org.nflash.server;

import java.util.Map;
import org.nflash.runner.NflashRunner;

public interface QueryHandler {
    
    public String startQuery(NflashRunner runner);
    
    public void stopQuery();

}