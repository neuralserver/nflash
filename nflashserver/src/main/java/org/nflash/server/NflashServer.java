package org.nflash.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class NflashServer {

    private static final int PORT = Integer.getInteger("port", 9080);

    // 
    // QueryId is mapped to QueryContainer
    // 
    private static final Map<String, QueryContainer> queryMap = new HashMap<String, QueryContainer>();
    
    // 
    // QueryId is mapped to websocket ChannelId.
    // 
    private static final Map<String, List<String>> queryContextMap = new HashMap<String, List<String>>();
    
    public static void main(String[] args) throws Exception {

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new NflashServerInitializer());

            Channel ch = b.bind(PORT).sync().channel();
            ch.closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
    
    public static Map<String, QueryContainer> getQueryMap() {
        return queryMap;
    }

    public static Map<String, List<String>> getQueryContextMap() {
        return queryContextMap;
    }
}