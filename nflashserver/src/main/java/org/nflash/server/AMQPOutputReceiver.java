package org.nflash.server;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

//
// Adapted from https://github.com/apache/qpid-jms/blob/master/qpid-jms-examples/src/main/java/org/apache/qpid/jms/example/HelloWorld.java
//
public class AMQPOutputReceiver {
    private static final String USER = System.getenv("ARTEMIS_USERNAME");
    private static final String PASSWORD = System.getenv("ARTEMIS_PASSWORD");
    
    private WebsocketMessageSender sender;
    
    public AMQPOutputReceiver(WebsocketMessageSender sender) { this.sender = sender; }
    
    public void start(String queueName) throws Exception {
        // The configuration for the Qpid InitialContextFactory has been supplied in
        // a jndi.properties file in the classpath, which results in it being picked
        // up automatically by the InitialContext constructor.
        Context context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("myFactoryLookup");

        Connection connection = factory.createConnection(USER, PASSWORD);
        connection.setExceptionListener(new MyExceptionListener());
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination queue = session.createQueue(queueName);
        MessageConsumer messageConsumer = session.createConsumer(queue);
        System.out.println("Connected. Starting to listen queue :: " + queueName);
        
        int timeout = 1000;
        try {
            while (true) {
                Message message = messageConsumer.receive(timeout);
                if (message != null) {
                    String messageBody = ((TextMessage) message).getText();
                    sender.sendText(messageBody, "OUTPUT", null, queueName); // sets the context null, sender will check what is the correct context
                }
            }
        } finally {
            connection.close();
        }
    }

    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("NflashServer :: AMQPOutputReceiver :: Connection ExceptionListener fired.");
            exception.printStackTrace(System.out);
        }
    }
}