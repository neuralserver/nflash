package org.nflash.server.model;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;

//
// Note that this is different than InputRequest (e.g. VisionInputRequest),
// because this commands the InputSourceDriver to begin feeding data into 
// wanted input-queue. InputRequests are actual input to those queues.
//
public class InputSourceDataRequest {
    private String inputSourceDriver; // name of the input-source-driver that will be used.
    private String dataSourceUri; // data source uri, e.g. URL, or folder name.
    private String targetQueue; // name of the queue that neural query is listening to.
    
    public InputSourceDataRequest() {}
    
    public static InputSourceDataRequest fromJson(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, InputSourceDataRequest.class);
    }
    
    public void setInputSourceDriver(String isd) {
        this.inputSourceDriver = isd;
    }
    
    public String getInputSourceDriver() {
        return this.inputSourceDriver;
    }

    public void setDataSourceUri(String dsuri) {
        this.dataSourceUri = dsuri;
    }
    
    public String getDataSourceUri() {
        return this.dataSourceUri;
    }

    public void setTargetQueue(String targetQ) {
        this.targetQueue = targetQ;
    }
    
    public String getTargetQueue() {
        return this.targetQueue;
    }

    public String toJsonString() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

}